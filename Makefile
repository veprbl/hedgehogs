VERSION=0.01

ifeq ($(TARGET),win32)
#CC=i586-mingw32msvc-cc
#CC=i686-linux-gnu-gcc
CC=i486-mingw32-gcc
#ARCH=i486-mingw32
#CC=cc
ARCH=i686-cygwin
	ifeq ($(SOUND_MODULE),oal)
		SOUND_LIBS = -lOpenAL32
	endif
	LIBS = -lgdi32 -lopengl32 -lwsock32 $(SOUND_LIBS)
else
CC=cc
ARCH=$(shell uname -m)
	ifeq ($(SOUND_MODULE),oal)
		SOUND_LIBS = -lopenal
	endif
	LIBS = -lm -lGL -lX11 $(SOUND_LIBS)
endif

SRC_DIR=./
BUILD_RELEASE_DIR=release-$(ARCH)/
BUILD_DEBUG_DIR=debug-$(ARCH)/
BUILD_DIR=$(BUILD_DEBUG_DIR)

WORKDIR = bin/

DEBUG_CFLAGS= -g
# /usr/local paths is requied for building in unix.
CFLAGS = $(DEBUG_CFLAGS) -L/usr/local/lib -I/usr/local/include/ -pg #-Wall -pedantic

LINUX_OBJ =\
	$(BUILD_DIR)l_fs.o \
	$(BUILD_DIR)l_gl.o \
	$(BUILD_DIR)l_main.o

WINDOWS_OBJ =\
	$(BUILD_DIR)w_fs.o \
	$(BUILD_DIR)w_gl.o \
	$(BUILD_DIR)w_main.o

ifeq ($(TARGET),win32)
ARCH_OBJ = $(WINDOWS_OBJ)
EXECUTABLE_EXT = .exe
else
ARCH_OBJ = $(LINUX_OBJ)
EXECUTABLE_EXT =
endif

EXECUTABLE_OUTPUT_PATH = $(WORKDIR)$@$(EXECUTABLE_EXT)

ifeq ($(SOUND_MODULE),oal)
	SOUND_OBJ = $(BUILD_DIR)snd_oal.o
else
	SOUND_OBJ = $(BUILD_DIR)snd_null.o
endif

OBJS =\
	$(BUILD_DIR)f_tga.o \
	$(BUILD_DIR)h_common.o \
	$(BUILD_DIR)h_console.o \
	$(BUILD_DIR)h_fs.o \
	$(BUILD_DIR)h_gl.o \
	$(BUILD_DIR)h_game.o \
	$(BUILD_DIR)h_geometry.o \
	$(BUILD_DIR)h_polygons.o \
	$(BUILD_DIR)h_textures.o \
	$(BUILD_DIR)h_net.o \
	$(BUILD_DIR)g_hedgehog.o \
	$(BUILD_DIR)g_polygons.o \
	$(BUILD_DIR)g_rod.o \
	$(BUILD_DIR)cl_main.o \
	$(BUILD_DIR)net_bsd.o \
	$(SOUND_OBJ) \
	$(ARCH_OBJ)

TESTS_DIR = tests/

all : hedgehogs

clean : 
	rm -rf $(BUILD_DIR)

run : hedgehogs
ifeq ($(TARGET),win32)
	exec ./w
else
	exec ./r
endif

debug : hedgehogs
	exec ./d

#include tests/Makefile

DO_CC = $(CC) $(CFLAGS) -o $@ -c $<

hedgehogs : $(BUILD_DIR) $(OBJS)
	$(CC) -o $(EXECUTABLE_OUTPUT_PATH) $(CFLAGS) $(OBJS) $(LIBS)

$(BUILD_DIR) :
	mkdir $(BUILD_DIR)

$(BUILD_DIR)f_tga.o : $(SRC_DIR)hcommon/f_tga.c
	$(DO_CC) 

$(BUILD_DIR)h_common.o : $(SRC_DIR)hcommon/h_common.c
	$(DO_CC)

$(BUILD_DIR)h_console.o : $(SRC_DIR)hcommon/h_console.c
	$(DO_CC)

$(BUILD_DIR)h_fs.o : $(SRC_DIR)hcommon/h_fs.c
	$(DO_CC)

$(BUILD_DIR)h_game.o : $(SRC_DIR)hcommon/h_game.c
	$(DO_CC)

$(BUILD_DIR)h_geometry.o : $(SRC_DIR)hcommon/h_geometry.c
	$(DO_CC)

$(BUILD_DIR)h_gl.o : $(SRC_DIR)hcommon/h_gl.c
	$(DO_CC)

$(BUILD_DIR)h_net.o : $(SRC_DIR)hcommon/h_net.c
	$(DO_CC)

$(BUILD_DIR)h_polygons.o : $(SRC_DIR)hcommon/h_polygons.c
	$(DO_CC)

$(BUILD_DIR)h_textures.o : $(SRC_DIR)hcommon/h_textures.c
	$(DO_CC)

$(BUILD_DIR)g_hedgehog.o : $(SRC_DIR)game/g_hedgehog.c
	$(DO_CC)

$(BUILD_DIR)g_polygons.o : $(SRC_DIR)game/g_polygons.c
	$(DO_CC)

$(BUILD_DIR)g_rod.o : $(SRC_DIR)game/g_rod.c
	$(DO_CC)

$(BUILD_DIR)net_bsd.o : $(SRC_DIR)hcommon/net_bsd.c
	$(DO_CC)

$(BUILD_DIR)snd_oal.o : $(SRC_DIR)hcommon/snd_oal.c
	$(DO_CC)

$(BUILD_DIR)snd_null.o : $(SRC_DIR)hcommon/snd_null.c
	$(DO_CC)

$(BUILD_DIR)cl_main.o : $(SRC_DIR)client/cl_main.c
	$(DO_CC)

$(BUILD_DIR)l_fs.o : $(SRC_DIR)linux/l_fs.c
	$(DO_CC)

$(BUILD_DIR)l_gl.o : $(SRC_DIR)linux/l_gl.c
	$(DO_CC)

$(BUILD_DIR)l_main.o : $(SRC_DIR)linux/l_main.c
	$(DO_CC)

$(BUILD_DIR)w_fs.o : $(SRC_DIR)win32/w_fs.c
	$(DO_CC)

$(BUILD_DIR)w_gl.o : $(SRC_DIR)win32/w_gl.c
	$(DO_CC)

$(BUILD_DIR)w_main.o : $(SRC_DIR)win32/w_main.c
	$(DO_CC)
