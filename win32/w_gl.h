#ifndef __W_GL_H_
#define __W_GL_H_

#include "windows.h"
#include "../hcommon/h_gl.h"

typedef struct
{
	void		*WndProc;

	HINSTANCE	hInst;
	HDC			hDC;
	HGLRC		hRC;
	HWND		hWnd;
	HGLRC		hGLRC;

//	HINSTANCE	hLibGL;

	hboolean	software_render;
} glw_state_t;

extern	glw_state_t	glw_state;

#endif
