#include "../hcommon/h_console.h"
#include "w_gl.h"

glw_state_t glw_state;

static TCHAR WND_CLASS[] = TEXT("QHWND");
static TCHAR WND_TITLE[] = TEXT("Quake Hedgehogs");

int VID_CreateWindow( int width, int height, hboolean fullscreen )
{
	WNDCLASS	wc;
	RECT		r;
	int		style;
	int		exstyle;
	
	Con_Print("Registering window class!\n");

	wc.style	= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.cbClsExtra	= 0;
	wc.cbWndExtra	= 0;
	wc.hInstance	= glw_state.hInst;
	wc.hIcon	= 0;
	wc.lpfnWndProc	= glw_state.WndProc;
	wc.hCursor	= LoadCursor(0, IDC_ARROW);
	wc.hbrBackground	= (void *)COLOR_GRAYTEXT;
	wc.lpszMenuName	= 0;
	wc.lpszClassName	= WND_CLASS;

	if (!RegisterClass(&wc))
	{
		Con_Printf("Error registering window class!\n");
		//return false;
	}

	if (fullscreen)
	{
		exstyle = WS_EX_TOPMOST;
		style = WS_POPUP | WS_VISIBLE;
	}
	else
	{
		exstyle = 0;
		style = WS_OVERLAPPED | WS_BORDER | WS_CAPTION | WS_SYSMENU | WS_VISIBLE;
	}

	r.left = 0;
	r.top = 0;
	r.right = width;
	r.bottom = height;

	if (!AdjustWindowRect(&r, style, false))
		return false;

	width = r.right - r.left;
	height = r.bottom - r.top;

	Con_Printf("Creating window...\n");

	glw_state.hWnd = CreateWindowEx(exstyle,
					WND_CLASS,
					WND_TITLE,
					style,
					0, 0, width, height,
					NULL, NULL,
					glw_state.hInst, NULL);

	if (!glw_state.hWnd)
	{
		Con_Printf("Can' create window!\n");
		return false;
	}

	glw_state.hDC = GetDC(glw_state.hWnd);
	if (!glw_state.hDC)
	{
		Con_Printf("Can't get DC!\n");
		return false;
	}

	ShowWindow(glw_state.hWnd, SW_SHOW);

	SetForegroundWindow(glw_state.hWnd);
	SetFocus(glw_state.hWnd);

	return (int)glw_state.hWnd;
}

hboolean VID_SetMode( int width, int height, int depth )
{
	DEVMODE dm;

	memset(&dm, 0, sizeof(dm));

	Con_Printf("\nSetting video mode (%ix%i @ %i)\n", width, height, depth);
	
	dm.dmSize		= sizeof(dm);
	dm.dmPelsWidth	= width;
	dm.dmPelsHeight	= height;
	dm.dmBitsPerPel	= depth;
	dm.dmFields		= DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

	if (ChangeDisplaySettings(&dm, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
	{
		Con_Print("Error setting video mode!\n");
		Con_Print("Setting windowed mode.\n");
		
		gl_state.fullscreen = false;
		return false;
	}
	return true;
}

hboolean VID_CreateRenderingContext(int depth)
{
	PIXELFORMATDESCRIPTOR pfd;
	int PixelFormat;

	memset(&pfd, 0, sizeof(pfd));
	pfd.nSize		= sizeof(pfd);
	pfd.nVersion		= 1;
	pfd.dwFlags		= PFD_DRAW_TO_WINDOW
		| PFD_SUPPORT_OPENGL
		| PFD_DOUBLEBUFFER;
	pfd.iPixelType		= PFD_TYPE_RGBA;
	pfd.cColorBits		= depth;
	pfd.cDepthBits		= 16;
	pfd.cStencilBits	= 0;
	pfd.iLayerType		= PFD_MAIN_PLANE;

	Con_Print("Choosing PixelFormat...\n");
	
	if (!(PixelFormat = ChoosePixelFormat(glw_state.hDC, &pfd)))
	{
		Con_Print("Error creating rendering context!\n");
		return false;
	}

	Con_Print("Setting PixelFormat...\n");
	if (!SetPixelFormat(glw_state.hDC, PixelFormat, &pfd))
	{
		Con_Print("Error setting PixelFormat!\n");
		return false;
	}

	Con_Print("Getting PixelFormat...\n");

	PixelFormat = GetPixelFormat(glw_state.hDC);

	Con_Print("Calling DescribePixelFormat\n");

	memset(&pfd, 0, sizeof(pfd));
	DescribePixelFormat(glw_state.hDC, PixelFormat, sizeof(pfd), &pfd);

	Con_Printf("	PFD is R%iG%iB%iA%i @%i\n",
		   pfd.cRedBits,
		   pfd.cGreenBits,
		   pfd.cBlueBits,
		   pfd.cAlphaBits,
		   pfd.cColorBits);
	Con_Printf("	Stencil bits: %i\n", pfd.cStencilBits);
	Con_Printf("	DepthBits (Z-Buffer): %i\n", pfd.cDepthBits);
	Con_Printf("	PFD_DRAW_TO_WINDOW: %s\n",
		   PFD_DRAW_TO_WINDOW & pfd.dwFlags ? "Yes" : "No");
	Con_Printf("	PFD_SUPPORT_OPENGL: %s\n",
		   PFD_SUPPORT_OPENGL & pfd.dwFlags ? "Yes" : "No");
	Con_Printf("	PFD_DOUBLEBUFFER: %s\n\n",
		   PFD_DOUBLEBUFFER & pfd.dwFlags ? "Yes" : "No");

	Con_Print("Creating rendering context...\n");
	glw_state.hRC = wglCreateContext(glw_state.hDC);
	if (!glw_state.hRC)
	{
		Con_Print("Error creating rendering context!\n");
		return false;
	}

	Con_Print("Selecting rendering context...\n"); 
	if (!wglMakeCurrent(glw_state.hDC, glw_state.hRC))
	{
		Con_Print("Can't set rendering context!\n");
		return false;
	}

	return true;
}

hboolean VID_Init()
{
	if (!VID_CreateWindow(gl_state.width,
			      gl_state.height,
			      gl_state.fullscreen))
		return false;
	if (gl_state.fullscreen)
		if (!VID_SetMode(gl_state.width, gl_state.height, 32))
			return false;
	if (!VID_CreateRenderingContext(24))
		return false;

	hgl_init();

	return true;
}

int VID_Shutdown()
{
	Con_Print("Restoring rendering context...\n"); 
	if (!wglMakeCurrent(glw_state.hDC, 0))
	{
		Con_Print("Error restoring rendering context!\n");
		return false;
	}

	Con_Print("Deleting rendering context...\n"); 
	if (!wglDeleteContext(glw_state.hRC))
	{
		Con_Print("Error deleting rendering context!\n");
		return false;
	}

	if (glw_state.hDC)
	{
		if (!ReleaseDC(glw_state.hWnd, glw_state.hDC))
		{
			Con_Print("Can't release device!\n");
			return false;
		}
		glw_state.hDC = 0;
	}
	if (glw_state.hWnd)
	{
		DestroyWindow(glw_state.hWnd);
		glw_state.hWnd = 0;
	}
	if (gl_state.fullscreen)
	{
		ChangeDisplaySettings(NULL, 0);
		gl_state.fullscreen = false;
	}
	return 1;
}

void	hgl_font_init()
{
	HFONT	font, fontInfo;
	main_font = glGenLists(96);
	fontInfo = CreateFont(-10, 0, 0, 0, 0, 0, 0, 0, ANSI_CHARSET,
			      OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS,
			      ANTIALIASED_QUALITY, FF_DONTCARE|DEFAULT_PITCH,
			      TEXT("Terminal")); 
	if (fontInfo == NULL) {
		Con_Printf("error loading font");
	}

	font = SelectObject(glw_state.hDC, fontInfo);
	wglUseFontBitmaps(glw_state.hDC, 32, 96, main_font);
	SelectObject(glw_state.hDC, font);
	DeleteObject(fontInfo);
}
