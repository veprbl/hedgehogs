#include <stdio.h>
#include <stdlib.h>
#include <GL/gl.h>

#include "../hcommon/h_console.h"
#include "windows.h"
#include "../hcommon/h_gl.h"
#include "w_gl.h"

#include "../hcommon/h_polygons.h"

extern	polygon_t	*map;

static __int64/*LARGE_INTEGER*/	last_time, new_time, time = 0, freq;
extern hboolean down;
int fup();
extern int	mouse_x, mouse_y;

long WINAPI WndProc (HWND	hWnd,
		     UINT	uMsg,
		     WPARAM	wParam,
		     LPARAM	lParam)
{
	switch (uMsg)
	{
	case WM_PAINT:
	{
		HGLRC hglrc = wglGetCurrentContext();
		char buf[200];
		static int frame_count = 0;
		static float	fps = 0;

		if (time == 0)
		{
			QueryPerformanceCounter(&time);
		}

		QueryPerformanceCounter(&new_time);

		if ((new_time - last_time) >= freq)
		{
			fps = frame_count*freq/(new_time - last_time);
			last_time = new_time;
			frame_count = 0;
		}

/*		do
		{
			QueryPerformanceCounter(&time);
		}
		while(time <= new_time + (freq / 30.0));*/

		glClear(GL_COLOR_BUFFER_BIT);

		glColor3f(1.0f, 1.0f, 1.0f);
		sprintf(buf, "FPS: %.1f", fps);
		hgl_print(20, 20, buf);

		Game_ProcessObjects((new_time - time)*30/freq*10);
		time = time + ((new_time - time)*30/freq)*freq/30;

		Hcommon_Frame();
		Game_DrawObjects();

		frame_count++;
		SwapBuffers(glw_state.hDC);

		return 0;
	}

	case WM_LBUTTONDOWN:
	{
		point_t p = {LOWORD(lParam), HIWORD(lParam)};
		shoot(&p);
		return 0;
	}

	case WM_KEYDOWN:
/*		printf("%i %i", wParam, lParam);*/

		switch(wParam)
		{
		case VK_ESCAPE:
			SendMessage(glw_state.hWnd, WM_CLOSE, 0, 0);
			break;
		case VK_SPACE:
			Con_Printf("polygon_tesselate() returned %i\n", polygon_tessellate(map));
			break;
		case VK_UP:
			fup();
			break;
		case VK_DOWN:
			down = 1;
			break;
		case VK_RETURN:
			{
				int		style;
				int		exstyle;

				gl_state.fullscreen = !gl_state.fullscreen;
				if (gl_state.fullscreen)
				{
					exstyle = WS_EX_TOPMOST;
					style = WS_POPUP | WS_VISIBLE;
					if (!VID_SetMode(gl_state.width, gl_state.height, 32))
						return false;
				}
				else
				{
					ChangeDisplaySettings(NULL, 0);
					exstyle = 0;
					style = WS_OVERLAPPED | WS_BORDER | WS_CAPTION | WS_SYSMENU | WS_VISIBLE;
				}
				SetWindowLong(glw_state.hWnd, GWL_STYLE, style);
				SetWindowLong(glw_state.hWnd, GWL_EXSTYLE, exstyle);
				SetWindowPos(glw_state.hWnd, NULL, 0, 0, gl_state.width, gl_state.height, 0);
				break;
			}
		}
		return 0;
	case WM_KEYUP:
		switch(wParam)
		{
		case VK_DOWN:
			down = 0;
			break;
		}
		return 0;

	case WM_SYSCOMMAND:
		if ((wParam == SC_SCREENSAVE) || (wParam == SC_MONITORPOWER))
			return 0;
		break;

	case WM_SIZE:
		hgl_reshape(LOWORD(lParam), HIWORD(lParam));
		return 0;

	case WM_CHAR:
		switch (wParam)
		{
		case 27:
			break;
		}
		return 0;

	case WM_CLOSE:
		PostQuitMessage(0);
		return 0;

	default:
		return DefWindowProc (hWnd, uMsg, wParam, lParam);
	}
	return DefWindowProc (hWnd, uMsg, wParam, lParam);
}

int WINAPI WinMain(HINSTANCE	hInstance,
		   HINSTANCE	hPrevInstance,
		   LPSTR	lpCmdLine,
		   int		nCmdShow)
{
	int	running = 1;
	MSG	msg;

	if (hPrevInstance)
		return 1;

	glw_state.hInst = hInstance;
	glw_state.WndProc = &WndProc;

	QueryPerformanceFrequency(&freq);

	Hcommon_Init(0, NULL);

	while (running)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				running = 0;
				break;
			}
			if(GetMessage(&msg, NULL, 0, 0))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
	}

	Hcommon_Shutdown();

	return 0;
}
