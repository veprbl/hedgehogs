#include <stdio.h>
#include <windows.h>
#include "../hcommon/h_console.h"
#include "../hcommon/h_fs.h"

void	*fs_mmap_file( const char *filename, file_handle_t *file_handle, fsize_t *size )
{
	HANDLE	*hFile;
	HANDLE	hMapping;
	char buf[MAXFILE];

	hFile = *file_handle = malloc(sizeof(hFile));

	*hFile = CreateFileA(&buf[0],
			     GENERIC_READ,
			     FILE_SHARE_READ | FILE_SHARE_WRITE,
			     NULL,
			     OPEN_EXISTING,
			     FILE_ATTRIBUTE_NORMAL,
			     0);

	if (*hFile == INVALID_HANDLE_VALUE)
	{

		sprintf(buf, "baseqhed/%s", filename);

		*hFile = CreateFileA(buf,
				     GENERIC_READ,
				     FILE_SHARE_READ | FILE_SHARE_WRITE,
				     NULL,
				     OPEN_EXISTING,
				     FILE_ATTRIBUTE_NORMAL,
				     0);

		if (*hFile == INVALID_HANDLE_VALUE)
		{
			Con_Printf("Can't open file '%s'!\n", filename);
			return 0;
		}
	}
	
	if (size)
	{
		*size = GetFileSize(*hFile, NULL);
	}

	hMapping = CreateFileMapping(*hFile, NULL, PAGE_READONLY, 0, 0,  NULL);

	if (hMapping == 0)
	{
		Con_Printf("Can't map file '%s'!\n", filename);
		return 0;
	}

	return MapViewOfFile(hMapping, FILE_MAP_READ, 0, 0, 0);
}

void	fs_munmap_file( char *buf, file_handle_t *file_handle )
{
	HANDLE	*hFile;

	hFile = file_handle;

	if (buf != NULL)
		UnmapViewOfFile(buf);
//	if (hFile != INVALID_HANDLE_VALUE)
//		CloseHandle(hFile);

	free(*file_handle);
}
