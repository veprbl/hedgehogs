#include <stdlib.h>
#include <AL/al.h>
#include <AL/alc.h>

#include "../hcommon/h_common.h"
#include "../hcommon/h_fs.h"
#include "../client/snd.h"
#include "snd_oal.h"

oal_state_t	oal_state;

hboolean	SND_Init()
{
	Con_Printf("=== SND Init ===\n");

	Con_Printf("Devices: %s\n", alcGetString(NULL, ALC_DEVICE_SPECIFIER));

	oal_state.device = alcOpenDevice(NULL);

	if (oal_state.device)
	{
		oal_state.context = alcCreateContext(oal_state.device, NULL);

		if (oal_state.context)
		{
			alcMakeContextCurrent(oal_state.context);
		}
		else
		{
			Con_Printf("SND: Error creating context!\n");
			return false;
		}
	}
	else
	{
		Con_Printf("SND: Error opening device!\n");
		return false;
	}

	alDistanceModel(AL_LINEAR_DISTANCE);

	h_open_pak();

	return true;
}

void	snd_shoot()
{
	static ALuint buf, src;
	file_handle_t handle;
	fsize_t size;
	static file_t	*wav;
	static hboolean	first = true;
	
	if (first)
	{
		first = false;

		wav = h_fopen("sound/weapons/laser2.wav", "r");

		alGenBuffers(1, &buf);
		alBufferData(buf, AL_FORMAT_MONO16, wav->buf, wav->size, 22050/2);
		
		if (alGetError() != AL_NO_ERROR)
		{
			Con_Printf("error!");
		}
		
		alGenSources(1, &src);
		
		alSourcei(src, AL_BUFFER, buf);
	}
	else
	{
		alSourceRewind(src);
	}
	alSourcePlay(src);
}



void	SND_Shutdown()
{
	Con_Printf("=== SND Shutdown ===\n");

	alcMakeContextCurrent(NULL);
	alcDestroyContext(oal_state.context);
	alcCloseDevice(oal_state.device);
}
