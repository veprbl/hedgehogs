#include <stdlib.h>
#include <string.h>
#include <GL/gl.h>
#include "h_fs.h"
#include "h_geometry.h"
#include "h_textures.h"
#include "h_gl.h"

gl_state_t	gl_state = {
#ifdef MSVC
	false, 640, 480, 3, 22
#else
	.fullscreen = false,
	.width = 640,
	.height = 480,
	.vid_x = 3,
	.vid_y = 22
#endif
};
GLuint	main_font;

void	hgl_init()
{
	glClearColor(0.5, 0.5, 0.5, 0);

	glViewport (0,0, 640, 480);
	glMatrixMode(GL_PROJECTION);
		glLoadIdentity ();
	glOrtho(0, 640, 0, 480, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
		glLoadIdentity ();
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);		// FIXME

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_ALPHA_TEST);
	glColor4f(1,1,1,1);

	glCullFace(GL_FRONT);

	hgl_font_init();
}

void	hgl_reshape( int width, int height )
{
	glViewport( 0, 0, width, height );
	glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
	glOrtho(0, width, height, 0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
}

void	hgl_print(float x, float y, char *text)
{
	glPushAttrib(GL_LIST_BIT);
	glListBase(main_font - 32);

	glDisable(GL_CULL_FACE);
	glRasterPos2f(x, y);
	glCallLists(strlen(text), GL_UNSIGNED_BYTE, text);
	glPopAttrib();
	glEnable(GL_CULL_FACE);
}

void	hgl_font_destroy()
{
	glDeleteLists(main_font, 96);
}

color_t*	color_ctor_rgb( float red, float green, float blue )
{
	return color_ctor_rgba(red, green, blue, 1.0f);
}

color_t*	color_ctor_rgba( float red, float green, float blue, float alpha )
{
	color_t	*new_color;

	new_color = malloc(sizeof(*new_color));

	new_color->red = red;
	new_color->green = green;
	new_color->blue = blue;
	new_color->alpha = alpha;

	return new_color;
}

void	color_dtor( color_t *color )
{
	free(color);
}

void	hglColor( color_t *color )
{
	glColor4fv((GLfloat*)color);
}

void	hglDrawRectangleSprite( const texture_t *tex, const point_t *pos,
				int tx, int ty, int h, int w )
{
	point_t	pt = { 0, 0 };
	if (pos == NULL)
	{
		pos = &pt;
	}

	glEnable(GL_TEXTURE_2D);
	texture_bind(tex);
	glBegin(GL_QUADS);

	glColor4f(1,1,1,1);
	glTexCoord2f(tx / (float)tex->width, ty / (float)tex->height);
	glVertex2f(pos->x, pos->y);
	glTexCoord2f((tx + w) / (float)tex->width, ty / (float)tex->height);
	glVertex2f(pos->x + w, pos->y);
	glTexCoord2f((tx + w) /(float)tex->width, (ty + h) /(float)tex->height);
	glVertex2f(pos->x + w, pos->y + h);
	glTexCoord2f(tx / (float)tex->width, (ty + h) / (float)tex->height);
	glVertex2f(pos->x, pos->y + h);

	glEnd();
	glDisable(GL_TEXTURE_2D);
}
