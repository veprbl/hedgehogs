#include <stdlib.h>
#include <string.h>
#include "f_tga.h"
#include "h_textures.h"
#include "h_console.h"

int SwapRGB( byte *image, unsigned int count, unsigned int bytes_per_pixel )
{
#ifdef __MSVC__
	printf("MSVC");
#endif
#ifndef __GNUC__ /* MSVC only */
	_asm
	{
		mov ebx, image
		mov ecx, count
		$loop:
		mov al, [ebx+0]
		mov ah, [ebx+2]
		mov [ebx+2], al
		mov [ebx+0], ah
		add ebx, bytes_per_pixel
		dec ecx
		jnz $loop
	}
#else
	byte	t;
	for(;count > 0; count--)
	{
		t = *image;
		*image = *(image+2);
		*(image+2) = t;
		image += bytes_per_pixel;
	}
#endif
}

int load_tga( byte *buf, unsigned int size, int *width, int *height, color_fmt_t *color_fmt, byte **image )
{
	tga_header_t	*tga_header;
	byte	repetition_count, num;
	byte	*pos, *end;
	unsigned int	color;
	int		row, col, i, bytes_per_pixel;

	if (size < sizeof(tga_header_t))
		return false;

	tga_header = malloc(sizeof(tga_header_t));
	memcpy(tga_header, buf, sizeof(tga_header_t));
	buf += sizeof(tga_header_t);
	
	if (tga_header->color_map != 0)
	{
		Con_Print("LoadTGA: It seems that image have color map.\n");
		return false;
	}
	if (((tga_header->image_type != 2) && (tga_header->image_type != 10)) || tga_header->image_descriptor & (1<<4))
	{
		Con_Print("LoadTGA: Unsupported image type.\n");
		return false;
	}

	tga_header->x_origin = LITTLE_ENDIAN_SHORT(tga_header->x_origin);
	tga_header->y_origin = LITTLE_ENDIAN_SHORT(tga_header->y_origin);
	*width = tga_header->width = LITTLE_ENDIAN_SHORT(tga_header->width);
	*height = tga_header->height = LITTLE_ENDIAN_SHORT(tga_header->height);
	Con_Printf("w,h : %i %i\n",*width, *height);
	bytes_per_pixel = tga_header->pixel_depth / 8;
	*color_fmt = (bytes_per_pixel == 4) ? BGRA : BGR;

	*image = malloc(*width * *height * bytes_per_pixel);

	if (tga_header->image_type == 2)
	{
		if (size < sizeof(tga_header_t) + *width * *height * bytes_per_pixel)
		{		
			free(*image);
			return false;
		}
		if (tga_header->image_descriptor & (1<<5))
		{
			memcpy(*image, buf, *width * *height * bytes_per_pixel);
		}
		else
		{
			for(row=*height-1; row>=0; row--)
			{
				memcpy(*image + row * (*width) * bytes_per_pixel, buf, *width * bytes_per_pixel);
				buf += *width * bytes_per_pixel;
			}
		}
	}
	else // RLE compressed
	{
		if (tga_header->image_descriptor & (1<<5))
		{
			pos = *image;
			end = *image + (*width) * (*height) * bytes_per_pixel;
			while(pos < end)
			{
				repetition_count = *(byte*)buf++;
				num = (repetition_count & 127) + 1;
				if (repetition_count & 128)
				{
					color = *(unsigned int*)buf;
					buf += bytes_per_pixel;
					for (i=0; i < num; i++)
					{
						*(unsigned int*)pos = color;
						pos += bytes_per_pixel;
					}
				}
				else
				{ // RAW packet
					memcpy(pos, buf, num * bytes_per_pixel);
					pos += num * bytes_per_pixel;
					buf += num * bytes_per_pixel;
				}
			}
		}
		else
		{
			row = (*height) - 1;
			col = 0;
			pos = *image + (*width) * (*height - 1) * bytes_per_pixel;

			while(row >= 0)
			{
				repetition_count = *(byte*)buf++;
				num = (repetition_count & 127) + 1;
				if (repetition_count & 128)
				{
					color = *(unsigned int*)buf;
					buf += bytes_per_pixel;

					for (i=0; i < num; i++)
					{
						if (bytes_per_pixel == 4)
						{
							*(unsigned int*)pos = color;
						}
						else
						{
							*pos = color & 0x0000FF;
							*(pos+1) = (color & 0x00FF00) >> 8;
							*(pos+2) = (color & 0xFF0000) >> 16;
						}
						
						pos += bytes_per_pixel;
					}
				}
				else
				{ // RAW packet
					while(col + num > *width)
					{
						memcpy(pos, buf, (*width - col) * bytes_per_pixel);
						pos -= (col + *width) * bytes_per_pixel;		// Move to previous line
						row--;
						num -= (*width - col);
						col = 0;
					}
					memcpy(pos, buf, num * bytes_per_pixel);
					pos += num * bytes_per_pixel;
					buf += num * bytes_per_pixel;
				}

				col += num;
				if (col >= *width)
				{
					col = 0;
					row--;
					pos -= *width * bytes_per_pixel * 2;
				}
			}
		}
	}

#ifndef GL_BGR
	if (*color_fmt == BGR)
	{
		SwapRGB(*image, (*width) * (*height), bytes_per_pixel);
		*color_fmt = RGB;
	}
#endif

#ifndef GL_BGRA
	if (*color_fmt == BGRA)
	{
		SwapRGB(*image, (*width) * (*height), bytes_per_pixel);
		*color_fmt = RGBA;
	}
#endif
	//return tga_header->image_type ;
	return 1;
}


