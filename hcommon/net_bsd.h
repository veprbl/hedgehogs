#ifdef WIN32
#include <winsock.h>
#else
#include <netinet/in.h>
#endif

#ifndef __L_NET_H_
#define __L_NET_H_

#ifdef WIN32
typedef SOCKET	net_socket_t;
#else
typedef int	net_socket_t;
#endif

#ifdef WIN32
#define CLOSE_SOCKET(fd)	closesocket(fd)
#define	IS_VALID_SOCKET(fd)	(fd != INVALID_SOCKET)
#else
#define CLOSE_SOCKET(fd)	close(fd)
#define	IS_VALID_SOCKET(fd)	(fd >= 0)
#define INVALID_SOCKET	-1
#endif

typedef struct net_info {
	struct sockaddr_in	addr;
	net_socket_t	socket;
} net_info_t;

#define	NET_INFO_T_DEFINED

#endif
