#include <stdlib.h>
#include <string.h>
#include "hash_table.h"

#include <assert.h>
#include <stdio.h>

int hash_table_test()
{
	hash_table_t	*ht = hash_table_ctor(DEF_HT_SIZE);

	assert(!hash_table_has_key(ht, "empty"));
	
	hash_table_set(ht, "full", "Hello, world!");
	{
		char buf[5] = "aaaa";
		int i, full_hash = hash_func("full", DEF_HT_SIZE);
		while(hash_func(buf, DEF_HT_SIZE) != full_hash)
		{
#define __INC_STR(i, next) \
			if (buf[(i)] == 'z') \
			{ \
			next \
			} else { \
			buf[(i)]++; \
			}
				  __INC_STR(3, 
					    __INC_STR(2, 
						      __INC_STR(1,
								__INC_STR(0,int a;))))
		}
		fprintf(stderr, "Found string with hash matching the hash of 'full'. It is '%s'.\n", buf);

		assert(hash_table_get(ht, "full"));
		assert(!hash_table_get(ht, buf));

		hash_table_set(ht, "full", "hi, men!");
		fprintf(stderr, "Program says: %s\n", hash_table_get(ht, "full"));
		assert(!strcmp(hash_table_get(ht, "full"), "hi, men!"));

		hash_table_set(ht, buf, "Who said lisp?");
		assert(!strcmp(hash_table_get(ht, "full"), "hi, men!"));
		assert(!strcmp(hash_table_get(ht, buf), "Who said lisp?"));		
	}

	fprintf(stderr, "========================================= [hash_table] OK\n");
	return 0;
}

hash_table_elem_t*	hash_table_elem_ctor( char *name, void *data )
{
	hash_table_elem_t	*new_hash_table_elem;

	new_hash_table_elem = malloc(sizeof(*new_hash_table_elem));

	new_hash_table_elem->next = NULL;
	new_hash_table_elem->name = name;
	new_hash_table_elem->data = data;

	return new_hash_table_elem;
}

hash_table_t*	hash_table_ctor( int size )
{
	hash_table_t	*new_hash_table;

	new_hash_table = malloc(sizeof(new_hash_table));

	new_hash_table->size = size;
	new_hash_table->count = 0;
	new_hash_table->table = calloc(size, sizeof(void*));
}

int	hash_func( char *name, int size )
{
	int	result = 0;

	while(*name)
	{
		result = (result + *name) % size;
		name++;
	}
	
	return result;
}

hboolean	hash_table_has_key( hash_table_t *hash_table, char *name )
{
	hash_table_elem_t	*elem;

	elem = hash_table->table[hash_func(name, hash_table->size)];

	while(elem)
	{
		if (strcmp(elem->name, name) == 0)
		{
			return true;
		}
		elem = elem->next;
	}

	return false;
}

void	hash_table_set( hash_table_t *hash_table, char *name, void *data )
{
	hash_table_elem_t	*elem;
	int	name_hash = hash_func(name, hash_table->size);

	if (hash_table->table[name_hash])
		// There is already element with same hash
	{
		elem = hash_table->table[name_hash];

		while(elem->next)
		{
			if (strcmp(elem->name, name) == 0)
			{
				elem->data = data;
				return;
			}
			elem = elem->next;
		}
		if (strcmp(elem->name, name) == 0)
		{
			elem->data = data;
			return;
		}

		elem->next = hash_table_elem_ctor(name, data);
	}
	else
	{
		hash_table->table[name_hash] = hash_table_elem_ctor(name, data);
	}
}

void*	hash_table_get( hash_table_t *hash_table, char *name )
{
	hash_table_elem_t	*elem;
	int	name_hash = hash_func(name, hash_table->size);

	if (hash_table->table[name_hash])
	{
		elem = hash_table->table[name_hash];

		while(elem)
		{
			if (strcmp(elem->name, name) == 0)
			{
				return elem->data;
			}
			elem = elem->next;
		}
	}
	return 0;
}
