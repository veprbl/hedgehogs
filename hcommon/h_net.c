#include <memory.h>

#include "h_net.h"

#include "../hcommon/h_common.h"
#include "../hcommon/h_game.h"

client_t	client[MAX_CLIENTS];
int		clients_count = 0;
net_packet_t	net_packet_queue[NET_PACKET_QUEUE_SIZE];

void	net_send_packet( client_t *to, net_packet_t *packet )
{
	char	buf[sizeof(net_packet_t)];

	buf[1] = packet->size - 1;
	buf[0] = packet->type;
	memcpy(&buf[2], &packet->data, packet->size);
	net_send_buf(to, buf, 2 + packet->size);
}

int	net_try_read_packet( char buf[], int buf_size, int sender )
{
	int j = 0, size = (int)buf[1] + 1, full_size = size + 2;

	if (buf_size >= full_size)
	{
		while(net_packet_queue[j].state != PS_FREE_SLOT)
		{
			j++;
			if (j == NET_PACKET_QUEUE_SIZE)
			{
				Con_Printf("NET: Dropped packet due to full queue.\n");
				return full_size;
			}
		}
	
		net_packet_queue[j].state = PS_READY;
		net_packet_queue[j].sender = sender;
		net_packet_queue[j].type = buf[0];
		net_packet_queue[j].size = size;

		memcpy(&net_packet_queue[j].data, &buf[2], size);

		return full_size;
	}
	else
	{
		return 0;
	}
}

void	NET_Process()
{
	int i = 0;

	NET_Recv();

	while((net_packet_queue[i].state != PS_READY) && (i < NET_PACKET_QUEUE_SIZE))
	{
		i++;
	}
	
	while(i < NET_PACKET_QUEUE_SIZE)
	{
		switch(net_packet_queue[i].type)
		{
		case PK_HELLO:
			if ((net_packet_queue[i].size == 1)
			    && (net_packet_queue[i].data[0] == QHED_VERSION))
			{
				Con_Printf("NET: player%i connected\n", net_packet_queue[i].sender);
			}
			else
			{
				/* Wrong client software, so kick him! */
				Con_Printf("NET: Kicked due to wrong response.\n", net_packet_queue[i].sender);
			}
			break;
			
		case PK_SYNC:
			if (net_packet_queue[i].size != sizeof(point_t) + sizeof(vector_t))
			{
				Con_Printf("NET: wrong packet size\n");
				return;
			}
			if (hedgehog[net_packet_queue[i].sender])
			{
				memcpy(hedgehog[net_packet_queue[i].sender]->body.pos, &net_packet_queue[i].data, sizeof(point_t));
				memcpy(hedgehog[net_packet_queue[i].sender]->body.speed, &net_packet_queue[i].data + sizeof(point_t), sizeof(vector_t));
			}
			else
			{
				Con_Printf("NET: no hedgehog %i\n", net_packet_queue[i].sender);
			}
			break;
		}

		{
			net_packet_t	sync_packet;
			sync_packet.type = PK_SYNC;
			sync_packet.size = sizeof(point_t) + sizeof(vector_t);
			memcpy(&sync_packet.data, hedgehog[local_hedgehog]->body.pos, sizeof(point_t));
			memcpy(&sync_packet.data + sizeof(point_t), hedgehog[local_hedgehog]->body.speed, sizeof(vector_t));
			net_send_packet(&client[net_packet_queue[i].sender], &sync_packet);
		}
		
		net_packet_queue[i].state = PS_FREE_SLOT;

		while((net_packet_queue[i].state != PS_READY) && (i < NET_PACKET_QUEUE_SIZE))
		{
			i++;
		}
	}
}

