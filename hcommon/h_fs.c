#include <stdlib.h>
#include <memory.h>

#include "h_fs.h"

pak_file_t	*first_file;
int	files_count;
void	*data;

void	h_open_pak()
{
	file_handle_t	pak_file;
	fsize_t		size;
	pak_header_t	*header;

	int	fileinfo_offset, fileinfo_size;

	Con_Printf("Loading pak0.pak...\n");

	data = fs_mmap_file("pak0.pak", &pak_file, &size);

	if (!data)
	{
		Con_Printf("PAK: Error while opening pak0.pak!\n");
		return;
	}

	header = (pak_header_t*)data;

	fileinfo_offset = LITTLE_ENDIAN_LONG(header->fileinfo_offset);
	fileinfo_size = LITTLE_ENDIAN_LONG(header->fileinfo_size);

	if (header->id != PAK_HEADER)
	{
		Con_Printf("PAK: Wrong pak file!\n");
		return;
	}

	if (size < fileinfo_offset + fileinfo_size)
	{
		Con_Printf("PAK: Pak file is too short!\n");
		return;
	}

	first_file = data + fileinfo_offset;
	files_count = fileinfo_size / sizeof(pak_file_t);
/*
	while(file <= data + fileinfo_offset + fileinfo_size
	      - sizeof(pak_file_t))
	{
		Con_Printf("Found %s\n", file->filename);
		file++;
	}
*/
}

file_t*	h_fopen( char *filename, char *mode )
{
	file_t	*new_file;
	pak_file_t	*file;
	int	i;

	new_file = malloc(sizeof(*new_file));

	if (strcmp(mode, "r") != 0)
	{
		return NULL; /* Unsupported */
	}

	for(i = 0, file = first_file; i < files_count; i++, file++)
	{
		if (strcmp(file->filename, filename) == 0)
		{
			Con_Printf("PAK0: Found %s\n", file->filename);

			new_file->buf = data + LITTLE_ENDIAN_LONG(file->file_offset);
			new_file->size = LITTLE_ENDIAN_LONG(file->file_size);
			new_file->file_handle = 0;
			return new_file;
		}
	}

	new_file->buf = fs_mmap_file( filename,
				      &new_file->file_handle,
				      &new_file->size );
	if (new_file->buf)
	{
		Con_Printf("Error opening '%s'!", filename);
		free(new_file);
		return NULL;
	}
	new_file->ptr = new_file->buf;

	return new_file;
}

void	h_fclose( file_t *file )
{
	if (file->file_handle)
	{
		fs_munmap_file( file->buf, &file->file_handle );
	}
	else
	{
		free(file->buf);
	}
	free(file);
}

fsize_t	h_fread( void *buf, fsize_t size, fsize_t nitems, file_t *file )
{
	fsize_t	read;

	read = (file->buf - file->ptr) % size;
	if (read > nitems)
	{
		read = nitems;
	}

	memcpy(buf, file->ptr, size * read);
	file->ptr += size * read;

	return read;
}

char	h_fgetc( file_t *file )
{
	file->ptr++;
	return *(char *)(file->ptr - 1);
}

char*	h_fgets( char *str, int num, file_t *file )
{
	char	c;

	num--;

	while((!h_feof(file)) && (num) && (c != '\n'))
	{
		c = h_fgetc(file);
		*str = c;
		str++;
		num++;
	}

	*str = 0;

	return str;
}

/*
fsize_t	h_fwrite( const void *buf, fsize_t size, fsize_t nitems, file_t *file )
{
	return 0;
}
*/

hboolean	h_feof( file_t *file )
{
	return file->ptr >= file->buf;
}
