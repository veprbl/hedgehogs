#include <stdlib.h>
#include <GL/gl.h>	//FIXME
#include "h_common.h"
#include "h_gl.h"
#include "h_polygons.h"

int	_point_half_plane( const point_t *prev, const point_t *next, const point_t *vertex )
{
	vector_t	v1, v2;

	vector_sub(next, prev, &v1);
	vector_sub(vertex, prev, &v2);

	return vector_half_plane(&v1, &v2);
}

int	_vertex_half_plane( const vertex_t *prev, const vertex_t *next, const vertex_t *vertex )
{
	return _point_half_plane(prev->_, next->_, vertex->_);
}

vertex_t*	vertex_ctor( point_t *point )
{
	vertex_t	*vertex;

	vertex = malloc(sizeof(*vertex));

	vector_inc_ref_count(point);

	vertex->_ = point;
	vertex->color = NULL;
	vertex->polygon = NULL;
	vertex->connected = NULL;
	vertex->next = NULL;
	vertex->prev = NULL;
	vertex->is_inside = 0;

	return vertex;
}

vertex_t*	vertex_ctor_from_coord( float x, float y )
{
	point_t	*point;

	point = point_ctor(x, y);

	return vertex_ctor(point);
}

void	vertex_dtor( vertex_t *vertex )
{
	if (vertex->polygon)
	{
		polygon_remove_vertex((polygon_t*)(vertex->polygon), vertex);
	}
	vector_dtor(vertex->_);
	free(vertex);
}

vertex_t*	vertex_make_clone( vertex_t *vertex )
{
	vertex_t	*clone;

	clone = vertex_ctor(vertex->_);

	clone->connected = vertex->connected;
	vertex->connected = clone;

	return clone;
}

vertex_t*	_next( vertex_t *vertex )
{
	return vertex->next;
}

vertex_t*	_prev( vertex_t *vertex )
{
	return vertex->prev;
}

void	polygon_create( polygon_t *polygon )
{
	polygon->color	= color_ctor_rgba(1.0f, 1.0f, 1.0f, 1.0f);
	polygon->vertices_count	= 0;
	polygon->first	= NULL;
	polygon->connected	= NULL;
}

polygon_t*	polygon_ctor()
{
	polygon_t	*polygon;

	polygon = malloc(sizeof(*polygon));

	polygon_create(polygon);

	return polygon;
}

void	polygon_dtor( polygon_t *polygon )
{
	while(polygon->first)
	{
		vertex_dtor(polygon->first);
	}

	free(polygon);
}

polygon_t*	polygon_make_clone( polygon_t *polygon )
{
	vertex_t	*vertex = polygon->first, *first = polygon->first;
	polygon_t	*clone;

	clone = polygon_ctor();

	clone->connected = polygon->connected;
	polygon->connected = clone;

	clone->color = polygon->color;

	do
	{
		polygon_add_vertex(clone, vertex_make_clone(vertex));
		vertex = _next(vertex);
	}
	while (vertex != first);

	return clone;
}

int	polygon_get_vertices_count( const polygon_t *polygon )
{
	return polygon->vertices_count;
}

int	polygon_find_vertex( const polygon_t *polygon, const vertex_t *what )
{
	int	i = 0;
	vertex_t	*vertex = polygon->first;
	
	while(vertex != what)
	{
		vertex = _next(vertex);
		i++;
		if (vertex == polygon->first)
		{
			return -1;
		}
	}
	
	return i;
}

hboolean	polygon_contains_vertex( const polygon_t *polygon,
					 const vertex_t *what )
{
	return polygon_find_vertex(polygon, what) != 0;
}

vertex_t*	polygon_offset_vertex( const polygon_t *polygon, int offset )
{
	int	i = 0;
	vertex_t	*vertex = polygon->first;

	if (polygon_get_vertices_count(polygon) == 0)
	{
		return (vertex_t*)NULL;		// polygon->first == NULL
	}

	offset %= polygon_get_vertices_count(polygon);
	if (offset >= 0)
	{
		while(offset--)
		{
			vertex = _next(vertex);
		}
	}
	else
	{
		while(offset++)
		{
			vertex = _prev(vertex);
		}
	}
	return vertex;
}

vertex_t*	polygon_add_vertex( polygon_t *polygon, vertex_t *vertex )
{
	vertex_t	*last = NULL;

	if (polygon->first)
	{
		last = _prev(polygon->first);
	}

	if (last)
	{
		last->next = vertex;
		vertex->next = polygon->first;
		vertex->prev = last;
		polygon->first->prev = vertex; 
	}
	else
	{
		polygon->first = vertex;
		vertex->next = vertex;
		vertex->prev = vertex;
	}

	vertex->polygon = (polygon_t*)polygon;

	vertex->id = polygon->vertices_count++;

	return vertex;
}

void	polygon_remove_vertex( polygon_t *polygon, vertex_t *vertex )
{
	if (vertex->polygon != polygon)
	{
		Con_Printf("can't remove vertex from foregin polygon!\n");
	}

	if ((vertex_t*)polygon->first == vertex)
	{
		polygon->first = _next(vertex);
	}

	polygon->vertices_count--;
	if (!polygon->vertices_count)
	{ 
		polygon->first = (vertex_t*)NULL;
	}
	else
	{
		_prev(vertex)->next = _next(vertex);
		_next(vertex)->prev = _prev(vertex);
	}

	vertex->next = NULL;
	vertex->prev = NULL;
	vertex->polygon = NULL;
}

void	polygon_reverse(polygon_t *polygon)
{
	vertex_t	*first = polygon->first, *vertex = first, *temp;

	do {
		temp = vertex->next;
		vertex->next = vertex->prev;
		vertex->prev = temp;

		vertex = _prev(vertex); // We move "backward" which is old "forward"
	} while (vertex != first);
}

hboolean	_convex_is_inside_polygon( const polygon_t *polygon,
					   const point_t *point )
{
	vertex_t	*first, *vertex, *next;

	if (polygon_get_vertices_count(polygon) < 3)
	{
		return false;
	}

	first = polygon->first;
	vertex = first;
	next = _next(vertex);

	do {
		if (_point_half_plane(vertex->_, next->_, point) == COUNTER_CLOCKWISE)
		{
			return false;
		}
		vertex = next;
		next = _next(next);
	} while (vertex != first);
	return true;
}

polygon_t*	is_inside_polygon(polygon_t *polygon, const point_t *point)
{
	if (polygon->connected)
	{
		do
		{
			if (_convex_is_inside_polygon(polygon, point))
			{
				return polygon;
			}
			polygon = polygon->connected;
		}
		while(polygon);

		return NULL;
	}
	else
	{
		return _convex_is_inside_polygon(polygon, point) ? polygon : NULL;
	}
}

void	_convex_polygon_draw(const polygon_t *polygon)
/* This draws correctly only convex, clockwise oriented polygons */
{
	vertex_t	*first, *vertex;
	char	buf[100];

	if (!polygon->first)
	{
		return;
	}

	first = vertex = _next(polygon->first);

	if (polygon->color)
	{
		polygon->color->red = 1.0f * ((int)polygon%5/5.0);
		polygon->color->green = 1.0f * ((int)polygon%7/7.0);
		polygon->color->blue = 1.0f * ((int)polygon%3/3.0);
		hglColor(polygon->color);
	}
//	glEnable(GL_TEXTURE_2D);
//	glBegin(GL_TRIANGLE_FAN);
	glBegin(GL_LINE_STRIP);
	do {
		if (vertex->color)
		{
			hglColor(vertex->color);
		}
		glTexCoord2f(vertex->_->x/100, vertex->_->y/100);
		glVertex2f(vertex->_->x, vertex->_->y);
		if (polygon->color)
		{
			hglColor(polygon->color);
		}
		vertex = _next(vertex);
	} while (vertex != first);
	glEnd();
//	glDisable(GL_TEXTURE_2D);
/*	do {
		glColor3f(1,1,1);
		if (vertex->is_inside)
		snprintf(buf, 100, "[%i]", vertex->id);
		else
		snprintf(buf, 100, "(%i)", vertex->id);
		hgl_print(vertex->_->x, vertex->_->y, buf);
		vertex = _next(vertex);
	} while (vertex != first);
*/
}

void	polygon_draw(const polygon_t *polygon)
{
	vertex_t	*vertex, *first;


	/*polygon_tessellate(polygon);*/
	if (polygon->connected)
	{
		polygon = polygon->connected;
	}

	do
	{
		_convex_polygon_draw(polygon);
	}
	while(polygon = polygon->connected);
}

hboolean	polygon_orientation(polygon_t *polygon)
{
	vertex_t	*first = polygon->first, *vertex = first, *bottom = first;
	coord_t	max_y;
	
	if (!first)
	{
		return false;
	}

	max_y = bottom->_->y;

	do {
		vertex = _next(vertex);
		if (max_y <= vertex->_->y)
		{
			max_y = vertex->_->y;
			bottom = vertex;
		}
	} while(vertex != first);

	return _vertex_half_plane(_prev(bottom), bottom, _next(bottom));
}

// TODO: untested!
coord_t	polygon_area( const polygon_t *polygon )
{
	vertex_t	*vertex;
	coord_t	area = 0.0;

	if (polygon_get_vertices_count(polygon) < 3)
	{
		return 0.0f;
	}

	FOR_EACH_VERTEX(vertex, polygon)
	{
		//      |i  j  k|
		//  det |x1 y1 0| = k * (x1 * y2 - x2 * y1)
		//      |x2 y2 0|

		area += (_next(vertex)->_->x * vertex->_->y - vertex->_->x * _next(vertex)->_->y) / 2;
	}
	NEXT;

	return area;
}

// TODO: untested!
void	polygon_find_center_of_mass( const polygon_t *polygon,
				     vector_t *result )
{
	vertex_t	*vertex;

	vector_set(result, 0.0, 0.0);

	FOR_EACH_VERTEX(vertex, polygon)
	{
		vector_inc(result, vertex->_);
	}
	NEXT;

	vector_mul(result, 1.0/polygon_get_vertices_count(polygon), result);
}

int	_mark_outside_vertices(vertex_t *first, vertex_t *second, hboolean mark)
{
	int	num_marked = 0;
	vertex_t	*vertex = _next(first);

	for(; vertex != first; vertex = _next(vertex))
	{
		if (vertex->is_inside)
		{
			if (	_vertex_half_plane(first, second, vertex) == COUNTER_CLOCKWISE	)
			{
				if (mark)
				{
					vertex->is_inside = 0;
/*					printf("marked vertex %i\n", vertex->id);*/
				}
				num_marked++;
			}
		}
	}

	return num_marked;
}

void	_polygon_orient( polygon_t *polygon )
{
	if (polygon_orientation(polygon) == COUNTER_CLOCKWISE)
	{
		polygon_reverse(polygon);
		printf("reversed\n");
	}
}

int	polygon_tessellate(polygon_t *polygon)
{
	int	num_polygons = 0,
		inside_vertices_count;
	vertex_t	*first, *second, *third, *vertex, *prev, *next;
	polygon_t	*new_polygon;

	_polygon_orient(polygon);

	if (polygon_get_vertices_count(polygon) <= 3)
	{
		return 0;
	}

	polygon = polygon_make_clone(polygon);
	
	vertex = polygon->first;
	prev = _prev(vertex);
	next = _next(vertex);

	while(true) {
		if (polygon_get_vertices_count(polygon) <= 3)
		{
			break;
		}

		first = vertex;

		while ((_vertex_half_plane(prev, vertex, next) != CLOCKWISE) ||
		       (_vertex_half_plane(_prev(prev), prev, vertex) != COUNTER_CLOCKWISE))
		{
			prev = vertex;
			vertex = next;
			next = _next(next);

			if (vertex == first)
			{
				return num_polygons;
			}
		}

		do {
			first = vertex;
			second = _next(first);
			third = _next(second);

			vertex = _next(third);
			while(vertex != first)
			{
				vertex->is_inside = 1;
				vertex = _next(vertex);
			}

			inside_vertices_count = polygon_get_vertices_count(first->polygon) - 3;

			inside_vertices_count -= _mark_outside_vertices(first, second, true);
			inside_vertices_count -= _mark_outside_vertices(second, third, true);
			
			do
			{
				vertex = _next(vertex);
			}
			while (_vertex_half_plane(vertex, _next(vertex), _next(_next(vertex))) == COUNTER_CLOCKWISE);
		} while (_mark_outside_vertices(third, first, false) != inside_vertices_count);

		prev = third;
		vertex = _next(prev);
	
		do {
			/* vertex vertex can't be inside our polygon anymore */
			inside_vertices_count--;

			inside_vertices_count -= _mark_outside_vertices(prev, vertex, true);

			if (_mark_outside_vertices(vertex, first, false) != inside_vertices_count)
			{
				break;
			}

			prev = vertex;
			vertex = _next(vertex);
		} while (vertex != first);

/*		printf("new poly %i - %i\n", first->id, prev->id);*/
		num_polygons++;

		new_polygon = polygon_ctor();

		new_polygon->connected = polygon->connected;
		polygon->connected = new_polygon;

		color_dtor(new_polygon->color);
		new_polygon->color = polygon->color;

		vertex = first;
		while(1)
		{
			polygon_add_vertex(new_polygon, vertex_make_clone(vertex));
/*			printf(" %i;",vertex->id);*/
			next = _next(vertex);
			if ((vertex != first) && (vertex != prev))
			{
				polygon_remove_vertex(polygon, vertex);
				vertex_dtor(vertex);
			}
			if (vertex == prev)
			{
				break;
			}
			vertex = next;
		}
		next = _next(vertex);
		prev = _prev(vertex);
	}

	return num_polygons;
}

void	polygon_find_nearest_normal( const polygon_t *polygon,
				     const point_t *point,
				     vector_t *normal )
{
	vertex_t	*vertex;
	vector_t	edge_vec, norm, min_norm, radius, proj;
	coord_t	min_len = -1, len_sqr;

	if (polygon_get_vertices_count(polygon) < 3)
	{
		return;
	}

	FOR_EACH_VERTEX(vertex, polygon)
	{
		vector_sub(_next(vertex)->_, vertex->_, &edge_vec);
		vector_normal(&edge_vec, &norm);

		vector_sub(point, vertex->_, &radius);

		vector_project(&norm, &radius, &proj);

		/*
		{
			point_t v;

			vector_copy(&v, vertex->_);
			vector_inc(&v, &proj);
			glBegin(GL_LINE_STRIP);
			glColor3f(v.x/200,v.y/200,0);

			glVertex2f(v.x, v.y);
			glVertex2f(vertex->_->x, vertex->_->y);
			glVertex2f(_next(vertex)->_->x, _next(vertex)->_->y);
			glColor3b(255,255,255);
			glEnd();
		}
		*/

		len_sqr = vector_len_sqr(&proj);
		if ((len_sqr < min_len) || (min_len < 0))
		{
			vector_copy(&min_norm, &proj);

			if (vector_half_plane(&edge_vec, &min_norm) == CLOCKWISE)
			{
				vector_mul(&min_norm, -1, &min_norm);
			}

			min_len = len_sqr;
		}
	}
	NEXT;

	vector_copy(normal, &min_norm);
}

vertex_t*	polygon_check_collision( const polygon_t *polygon,
					 const point_t *obj_pos,
					 const vector_t *obj_speed,
					 point_t *intersection_point)
/*
  @return  first vertex from intersecting edge
 */
/*  If you are not interested in intersection point you may just pass NULL. */
{
	vertex_t	*vertex, *nearest_vertex = NULL;
	point_t		new_pos, point;
	coord_t		min_dist_sqr;
	hboolean	first = true;

	if (polygon_get_vertices_count(polygon) < 2)
	{
		return NULL;
	}

	vector_add(obj_pos, obj_speed, &new_pos);

	FOR_EACH_VERTEX(vertex, polygon)
	{
		if (line_segment_intersects(obj_pos, &new_pos,
					    vertex->_, _next(vertex)->_,
					    &point))
		{
			/* TODO: replace with point to segment distance */
			coord_t dist_sqr = point_dist_sqr(&point, obj_pos);

			if (first || (min_dist_sqr > dist_sqr))
			{
				first = false;
				min_dist_sqr = dist_sqr;
				nearest_vertex = vertex;

				if (intersection_point)
				{
					vector_copy(intersection_point, &point);
				}
			}
		}
	}
	NEXT

	return nearest_vertex;
}
