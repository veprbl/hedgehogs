#include "h_common.h"

#ifndef __HASH_TABLE_H_
#define __HASH_TABLE_H_

typedef struct hash_table_elem_t
{
	char	*name;
	void	*data;
	struct hash_table_elem_t	*next;
} hash_table_elem_t;

typedef struct
{
	int	size;
	int	count;
	hash_table_elem_t	**table;
} hash_table_t;

#define	DEF_HT_SIZE	512

hash_table_t*	hash_table_ctor( int size );
hboolean	hash_table_has_key( hash_table_t *hash_table, char *name );
void	hash_table_set( hash_table_t *hash_table, char *name, void *elem );
void*	hash_table_get( hash_table_t *hash_table, char *name );

#endif
