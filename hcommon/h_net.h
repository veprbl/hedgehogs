#ifndef __H_NET_H_
#define __H_NET_H_

#define	NET_PORT	31337
#define	NET_PACKET_QUEUE_SIZE	32
#define MAX_CLIENTS	16
#define MAX_DATA_SIZE	256
#define MAX_NICK_LEN	16

#ifndef NET_INFO_T_DEFINED
typedef void	net_info_t;
#define	NET_INFO_T_DEFINED
#endif

typedef struct client {
	int	connected;
	net_info_t	*net_info;
	char	nick[MAX_NICK_LEN];
} client_t;

typedef enum {
	PK_HELLO,
	PK_SYNC,
	PK_PING,
	PK_PONG
} net_packet_type_t;

typedef enum {
	PS_FREE_SLOT,
	PS_RECIEVING,
	PS_READY,
	PS_PROCESSING
} net_packet_state_t;

typedef struct net_packet {
	int	sender, reciever;
	net_packet_type_t	type;
	net_packet_state_t	state;
	int	size;
	char	data[MAX_DATA_SIZE];
} net_packet_t;

extern client_t	client[MAX_CLIENTS];
extern int	clients_count;
extern net_packet_t	net_packet_queue[NET_PACKET_QUEUE_SIZE];

void	NET_Init();
void	NET_Recv();
void	NET_Process();
void	NET_Shutdown();

void	net_send_buf( client_t *to, void *buf, int size );
void	net_send_packet( client_t *to, net_packet_t *packet );
int	net_try_read_packet( char buf[], int buf_size, int sender );

#endif
