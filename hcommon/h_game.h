#include "h_gl.h"
#include "h_geometry.h"
#include "h_polygons.h"

#ifndef __H_GAME_H_
#define __H_GAME_H_

typedef void (*process_func_ptr_t) ( const void*, int );
typedef void (*draw_func_ptr_t) ( const void* );
typedef void (*collide_func_ptr_t) ( const void*, const void* );
typedef void (*check_collision_func_ptr_t) ( const void*, const point_t*, const vector_t* );

typedef struct body_type {
	process_func_ptr_t	process_func;
	draw_func_ptr_t		draw_func;
	collide_func_ptr_t	collide_func;
	check_collision_func_ptr_t	check_collision_func;
} body_type_t;

typedef struct body {
	const body_type_t	*type;
	float	mass, angular_mass;
	float	angle, angular_freq;
	point_t	*pos;
	vector_t	*speed, *accel;
} body_t;

typedef struct particle {
	vector_t	pos;
	vector_t	speed;
	int	size;
	color_t	color;
} particle_t;

#define CLASS_BODY( process, draw, collide, check_collision )	\
	{					\
		(process_func_ptr_t)process,	\
		(draw_func_ptr_t)draw,		\
		(collide_func_ptr_t)collide,		\
		(check_collision_func_ptr_t)check_collision	\
	}

void	Game_Init ();
void	Game_Shutdown ();
void	Game_DrawObjects ();

body_t*	body_ctor();
void	body_dtor( body_t *body );
void /*virtual*/	body_draw( const body_t *body );
void /*virtual*/	body_check_collision( body_t *body,
					      const vector_t *obj_pos,
					      const vector_t *obj_speed );
void	body_process( body_t *body, int ticks );
void	body_apply_impulse( body_t *self, body_t *other,
			    point_t *pos, vector_t *impulse );

void	shoot( point_t *t );

extern object_pool_t	game_bodies;

#include "../game/g_hedgehog.h"
#include "../game/g_rod.h"
#include "../game/g_polygons.h"

#endif
