#include <GL/gl.h>
#include "h_geometry.h"
#include "h_textures.h"
#include "h_common.h"

#ifndef __H_GL_H_
#define __H_GL_H_

typedef struct
{
	hboolean	fullscreen;

	int	width, height, vid_x, vid_y;
} gl_state_t;

extern	gl_state_t	gl_state;
extern	GLuint	main_font;

typedef struct color
{
	GLfloat	red, green, blue, alpha;
} color_t;

void	hgl_init();
void	hgl_print(float x, float y, char *text);
void	hgl_font_init();
color_t*	color_ctor_rgb( float red, float green, float blue );
color_t*	color_ctor_rgba( float red, float green, float blue, float alpha );
void	color_dtor( color_t *color );
void	hglColor( color_t *color );
void	hglDrawRectangleSprite( const texture_t *tex, const vector_t *pos,
				int tx, int ty, int h, int w );

#endif


