#include "h_common.h"
#include "h_geometry.h"
#include "h_gl.h"

#ifndef __H_POLYGONS_H_
#define __H_POLYGONS_H_

typedef struct vertex_t {
	point_t	*_;
	color_t	*color;
	int	is_inside;
	int	id;
	void	*polygon;
	struct vertex_t *connected;
	struct vertex_t	*next;
	struct vertex_t	*prev;
} vertex_t;

vertex_t*	vertex_ctor( point_t *point );
vertex_t*	vertex_ctor_from_coord( coord_t x, coord_t y );
void	vertex_dtor( vertex_t *vertex );

typedef struct polygon {
	color_t		*color;
	int		vertices_count;
	vertex_t	*first;
	struct polygon	*connected;
} polygon_t;

#define	FOR_EACH_VERTEX(vertex, polygon)	{ int _f; for((_f = 1), (vertex = (polygon)->first); (vertex != (polygon)->first) || (_f); (_f = 0), (vertex = _next(vertex)))
#define NEXT	}
vertex_t*	_next( vertex_t *vertex );	/* This is only for iterating vertices */

void	polygon_create( polygon_t *polygon );
polygon_t*	polygon_ctor();
void	polygon_dtor( polygon_t *polygon );
int	polygon_get_vertices_count( const polygon_t *polygon );
int	polygon_find_vertex( const polygon_t *polygon, const vertex_t *what );
hboolean	polygon_contains_vertex( const polygon_t *polygon,
					 const vertex_t *what );
vertex_t*	polygon_offset_vertex( const polygon_t *polygon, int offset );
vertex_t*	polygon_add_vertex( polygon_t *polygon, vertex_t *vertex );
void	polygon_remove_vertex( polygon_t *polygon, vertex_t *vertex );
void	polygon_reverse( polygon_t *polygon );
polygon_t*	is_inside_polygon( polygon_t *polygon, const point_t *point );
void	polygon_draw( const polygon_t *polygon );
hboolean	polygon_orientation( polygon_t *polygon );
coord_t	polygon_area( const polygon_t *polygon );
void	polygon_find_center_of_mass( const polygon_t *polygon,
				     vector_t *result );
int	polygon_tessellate( polygon_t *polygon );
void	polygon_find_nearest_normal( const polygon_t *polygon,
				     const point_t *point,
				     vector_t *normal );
vertex_t*	polygon_check_collision( const polygon_t *polygon,
					 const point_t *obj_pos,
					 const vector_t *obj_speed,
					 point_t *intersection_point );

#endif
