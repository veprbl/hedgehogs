#ifndef __H_CONSOLE_H_
#define __H_CONSOLE_H_

#define	CON_PRINTF_MAX	2048

void Con_Init();
void Con_Print(char *txt);
void Con_Printf(char *fmt, ...);

#endif
