#include <stdlib.h>
#include <memory.h>

#ifdef WIN32
#include <winsock.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <errno.h>
#endif

#include <fcntl.h>

#include "../hcommon/h_game.h"
#include "net_bsd.h"
#include "../hcommon/h_net.h"

#define	IS_VALID_CLIENT_SOCKET(client)	(client.connected && client.net_info \
					 && IS_VALID_SOCKET(client.net_info->socket))

net_socket_t	listener = INVALID_SOCKET;
int	addr_len = sizeof(struct sockaddr_in);

void	net_send_buf( client_t *to, void *buf, int size )
{
	send(to->net_info->socket, buf, size, 0);
}

void	net_kick_client( client_t *client )
{
	shutdown(client->net_info->socket, 2);
	CLOSE_SOCKET(client->net_info->socket);
	client->connected = false;
}

void	NET_Connect()
{
	net_info_t	*client_info;
	struct sockaddr_in	addr;
	net_socket_t	fd;

	Con_Printf("NET: Trying connect to 127.0.0.1\n");

	addr.sin_family	= AF_INET;
	addr.sin_port	= htons(NET_PORT);
	addr.sin_addr.s_addr	= htonl(INADDR_LOOPBACK);

	fd = socket(AF_INET, SOCK_STREAM, 0);

	if (!IS_VALID_SOCKET(fd))
	{
		Con_Printf("NET: Error creating socket!\n");
		return;
	}

	if (connect(fd, (struct sockaddr*)&addr, sizeof(addr)) < 0)
	{
		Con_Printf("NET: Error connecting!\n");
		return;
	}

	client_info = malloc(sizeof(*client_info));
	
	client_info->socket = fd;
	memcpy(&client_info->addr, (struct sockaddr*)&addr, sizeof(addr));
	
	client[0].connected = true;
	client[0].net_info = client_info;

	local_hedgehog = 1;
	hedgehog[0] = hedgehog_ctor();
}

hboolean	NET_ServerStart()
{
	struct sockaddr_in	addr;
	int	fd, reuse = 1;

	listener = socket(AF_INET, SOCK_STREAM, 0);

	if (!IS_VALID_SOCKET(listener))
	{
		Con_Printf("NET: Error creating socket!\n");
		return;
	}

	setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));

	addr.sin_family		= AF_INET;
	addr.sin_port		= htons(NET_PORT);
	addr.sin_addr.s_addr	= htonl(INADDR_ANY);

	if (bind(listener, (struct sockaddr*)&addr, sizeof(addr)) < 0)
	{
		Con_Printf("NET: Error binding socket!\n");

		CLOSE_SOCKET(listener);
		listener = INVALID_SOCKET;

		return false;
	}
	else
	{
		local_hedgehog = 0;
	}

	listen(listener, MAX_CLIENTS);

	return true;
}

void	NET_Init()
{
	int	i;

#ifdef WIN32
	{
		WSADATA wsa_data;
		
		if (WSAStartup(MAKEWORD(1, 0), &wsa_data) != 0)
		{
			Con_Printf("NET: Error initializing Winsock!");
			return;
		}
	}
#endif

	for(i = 0; i < MAX_CLIENTS; i++)
	{
		client[i].connected = false;
	}

	Con_Printf("=== NET_Init ===\n");

	if (!NET_ServerStart())
	{
		NET_Connect();
	}

	client[local_hedgehog].connected = true;
}

void	NET_Recv()
{
	struct sockaddr_in	addr;
	struct timeval	timeout;
	fd_set	socks;
	int	max_fd;
	int	i, readsocks;

	timeout.tv_sec = 0;
	timeout.tv_usec = 0;

	FD_ZERO(&socks);

	if (IS_VALID_SOCKET(listener))
	{
		FD_SET(listener, &socks);
	}
	max_fd = listener;

	for(i = 0; i < MAX_CLIENTS; i++)
	{
		if (IS_VALID_CLIENT_SOCKET(client[i]))
		{
			FD_SET(client[i].net_info->socket, &socks);
			if (client[i].net_info->socket > max_fd)
			{
				max_fd = client[i].net_info->socket;
			}
		}
	}

	do
	{
	readsocks = select(max_fd + 1, &socks, (fd_set*)0, (fd_set*)0, &timeout);
	} while ((readsocks < 0)
#ifndef WIN32
		 && (errno = EINTR)
#endif
		);

	if (readsocks < 0)
	{
		Con_Printf("NET: Error in select()!\n");
		return;
	}
	else if (readsocks == 0)
	{
		return;
	}

	if (FD_ISSET(listener, &socks))
	{
		net_info_t	*net_info;
		net_socket_t	fd;
		int	i = 0;

		fd = accept(listener, (struct sockaddr*)&addr, &addr_len);
		
		if (!IS_VALID_SOCKET(fd))
		{
			Con_Printf("NET: Error in accept()!\n");
			return;
		}
			
		Con_Printf("NET: connection from %s\n", 
			   inet_ntoa(addr.sin_addr));

		while(client[i].connected)
		{
			i++;
			if (i == MAX_CLIENTS)
			{
				Con_Printf("NET: Kicked due to client limit.\n");
				CLOSE_SOCKET(fd);
				return;
			}
		}

		Con_Printf("NET: player%i joined\n", i);

		net_info = (net_info_t*)malloc(sizeof(*net_info));

		net_info->socket = fd;
		memcpy(&net_info->addr, (struct sockaddr*)&addr, sizeof(addr));

		client[i].connected = true;
		client[i].net_info = net_info;
		hedgehog[i] = hedgehog_ctor();

		{
			net_packet_t	hello_packet;
			hello_packet.type = PK_HELLO;
			hello_packet.size = 1;
			hello_packet.data[0] = QHED_VERSION;
			net_send_packet(&client[i], &hello_packet);
		}
	}

	for(i = 0; i < MAX_CLIENTS; i++)
	{
		if (IS_VALID_CLIENT_SOCKET(client[i]) &&
		    FD_ISSET(client[i].net_info->socket, &socks))
		{
			static char	buf[MAX_CLIENTS][sizeof(net_packet_t)];
			static int	buf_pos[MAX_CLIENTS] = {0};
			int	len, read;

			len = recv(client[i].net_info->socket,
				   &buf[i][buf_pos[i]],
				   sizeof(buf[i]) - buf_pos[i], 0);

			if (len < 0)
			{
				perror("recv");
				return;
			}
			buf_pos[i] += len;
			
			if (read = net_try_read_packet(buf[i], buf_pos[i], i))
			{
				memcpy(buf[i], &buf[i][read], buf_pos[i] - read);

				buf_pos[i] -= read;
			} 
		}
	}
}

void	NET_Shutdown()
{
	Con_Printf("=== NET_Shutdown ===\n");

	if (IS_VALID_SOCKET(listener))
	{
		if (shutdown(listener, 2) != 0)
		{
			Con_Printf("NET: Error shutting down listener socket!\n");
		}

		CLOSE_SOCKET(listener);
	}

#ifdef WIN32
	WSACleanup();
#endif
}
