#include "h_common.h"

#ifndef __H_FS_H_
#define __H_FS_H_

typedef	unsigned int	fsize_t;
typedef	void	*file_handle_t;

typedef	struct file {
	void	*buf, *ptr;
	fsize_t	size;
	file_handle_t	file_handle;
} file_t;

file_t*	h_fopen( char *filename, char *mode );
void	h_fclose( file_t *file );
fsize_t	h_fread( void *buf, fsize_t size, fsize_t nitems, file_t *file );
char	h_fgetc( file_t *file );
char*	h_fgets( char *str, int num, file_t *file );
fsize_t	h_fwrite( const void *buf, fsize_t size, fsize_t nitems, file_t *file );
hboolean	h_feof( file_t *file );

void	*fs_mmap_file( const char *filename, file_handle_t *file_handle,
		       fsize_t *size );
void	fs_munmap_file ( char *buf, file_handle_t *file_handle );

typedef struct {
	int	id;
	int	fileinfo_offset;
	int	fileinfo_size;
} pak_header_t;

#define	PAK_HEADER	(('P') | ('A' << 8) | ('C' << 16) | ('K' << 24))
#define	PAK_MAX_FILE_PATH	56

typedef struct {
	char	filename[PAK_MAX_FILE_PATH];
	int	file_offset, file_size;
} pak_file_t;

#endif
