#include <stdlib.h>
/* FIXME: define _USE_MATH_DEFINES in msvc solution */
#include <math.h>
#include <GL/gl.h>

#include "h_common.h"
#include "h_game.h"
#include "h_polygons.h"
#include "h_textures.h"
#include "h_gl.h"

object_pool_t	game_bodies = OBJECT_POOL_INITIALIZER;

polygon_body_t *map_body;
polygon_t      *map;

int	mouse_x, mouse_y;

/*fixme*/
polygon_body_t *b;
polygon_t *p;

/*debug*/
hboolean	physics = true;

void	body_create( body_t *body, const body_type_t *body_type )
{
	body->type	= body_type;
	body->mass	= 1.0;
	body->angular_mass	= body->mass * 1;
	body->angle	= 0.0;
	body->angular_freq	= 0.0;

	body->pos	= point_ctor(300.0, 10.0);
	vector_inc_ref_count(body->pos);

	body->speed	= vector_ctor(0.0, 0.0);
	vector_inc_ref_count(body->speed);

	body->accel	= vector_ctor(0.0, 0.0);
	vector_inc_ref_count(body->accel);
}

body_t*	body_ctor( const body_type_t *body_type )
{
	body_t	*new_body;

	new_body = malloc(sizeof(*new_body));

	body_create(new_body, body_type);

	object_pool_push(&game_bodies, new_body);

	return new_body;
}

void	body_dtor( body_t *body )
{
	vector_dtor(body->pos);
	vector_dtor(body->speed);
	object_pool_free(&game_bodies, body);
}

void /*virtual*/	body_draw( const body_t *body )
{
	if (body->type->draw_func)
	{
		glPushMatrix();
		glTranslatef(body->pos->x, body->pos->y, 0);
		glRotatef(body->angle, 0, 0, 1);
       		body->type->draw_func(body);
		glPopMatrix();
	}
}

void /*virtual*/	body_check_collision( body_t *body,
					      const vector_t *obj_pos,
					      const vector_t *obj_speed )
{
	if (body->type->check_collision_func)
	{
       		body->type->check_collision_func((void*)body,
						 obj_pos, obj_speed);
	}
}

void	body_process( body_t *body, int ticks )
{
	vector_t	movement;

	if (body->type->process_func)
	{
       		body->type->process_func(body, ticks);

		if (physics)
		{
			vector_mul(body->speed, ticks / 1000.0, &movement);
			vector_inc(body->pos, &movement);

			body->angle += body->angular_freq * ticks / 1000.0;
		}
	}
}

void	gravitator_body_process( body_t *self, int ticks )
{
	vector_t	gravity_impulse;
	static vector_t	gravity_accel = {0.0, -9.8*100};
	body_t		*body;

	ITER_OBJECT_POOL(body, &game_bodies)
	{
		if ((body == self) || (body == map_body))
		{
			continue;
		}

		vector_copy(&gravity_impulse, &gravity_accel);
		vector_mul(&gravity_impulse,
			   ticks / 1000.0 * 15 * body->mass,
			   &gravity_impulse);

		body_apply_impulse(body, self, body->pos,
				   &gravity_impulse);
	}
	ITER_END
}

const body_type_t
GravitatorBody = CLASS_BODY(
	gravitator_body_process,
	NULL,
	NULL,
	NULL
	);

body_t*	gravitator_body_ctor( void )
{
	return body_ctor(&GravitatorBody);
}

void	polygon_body_draw( const polygon_body_t *self )
{
	polygon_draw(&self->polygon);
}

const body_type_t PolygonBody;

void	polygon_body_collide( const polygon_body_t *self, const body_t *other_body )
{
	vertex_t	*vertex;

	if (other_body->type == &PolygonBody)
	{
		FOR_EACH_VERTEX(vertex, &self->polygon)
		{
			
		}
		NEXT;
	}
}

void	body_apply_impulse( body_t *self, body_t *other,
			    point_t *pos, vector_t *impulse )
{
	vector_t	speed_delta, radius;

	vector_mul(impulse, 1/self->mass, &speed_delta);
	vector_inc(self->speed, &speed_delta);

	vector_sub(pos, self->pos, &radius);
	if (vector_len_sqr(&radius) > 0)
	{
		self->angular_freq -= vector_cross(impulse, &radius)
			/ self->angular_mass / M_PI * 180;
	}

	if (other)
	{
		vector_t	other_impulse;
		vector_mul(impulse, -1, &other_impulse);
		body_apply_impulse(other, NULL, pos, &other_impulse);
	}
}

const body_type_t
PolygonBody = CLASS_BODY(
	polygon_body_process,
	polygon_body_draw,
	polygon_body_collide,
	NULL
	);

polygon_body_t*	polygon_body_ctor()
{
	polygon_body_t	*new_polygon_body;

	new_polygon_body = malloc(sizeof(*new_polygon_body));

	body_create(&new_polygon_body->body, &PolygonBody);
	polygon_create(&new_polygon_body->polygon);

	object_pool_push(&game_bodies, new_polygon_body);

	return new_polygon_body;
}

char	down = 0;

int fup()
{
	hedgehog[local_hedgehog]->body.speed->y += 1000000;
	return 0;
}

void	shoot( point_t *t )
{
/*	snd_shoot();
	ray_of_death_ctor(t);*/
/*	vector_copy(hedgehog[local_hedgehog]->body.pos, t);*/
	vector_copy(b->body.pos, t);
	b->body.pos->y = 480 - b->body.pos->y;
}

#define sin(x) -sin(x*1.5+5)
void	Game_Init()
{
	float x;

	gravitator_body_ctor();

	b = polygon_body_ctor();
	p = &b->polygon;

	polygon_add_vertex(p, vertex_ctor_from_coord(-40, -20));
	polygon_add_vertex(p, vertex_ctor_from_coord(40, -20));
	polygon_add_vertex(p, vertex_ctor_from_coord(40, 20));
	polygon_add_vertex(p, vertex_ctor_from_coord(-40, 20));
	b->body.angle = 90;
	b->body.mass = 0.1;

//	object_pool_remove(&game_bodies, b);

	map_body = polygon_body_ctor();
	map_body->body.angular_freq = -0;
//	object_pool_remove(&game_bodies, map_body);

	map_body->body.mass = 1000000000000;
	map_body->body.angular_mass = 50000;
	vector_set(map_body->body.pos, 0, 0);
	map = &map_body->polygon;

	for(x = 10; x <= 630; x += 10.1)
	{
		polygon_add_vertex(map, vertex_ctor_from_coord(x, 150 - 150*sin(x/300*M_PI)));
	}

	x -= 10;

	for(; x >= 10; x -= 10)
	{
		polygon_add_vertex(map, vertex_ctor_from_coord(x, 180 - 150*sin(x/300*M_PI)));
	}

	polygon_tessellate(map);

	hedgehog_tex = texture_load("player.tga");
	hedgehog[local_hedgehog] = hedgehog_ctor();
	vector_set(hedgehog[local_hedgehog]->body.pos, 200, 300);
}

void	Game_Shutdown()
{
	object_pool_remove_all(&game_bodies);
}

#include <stdio.h> /* for snprintf */
void	Game_DrawObjects()
{
	int	i;
	body_t	*body;

	for(i = 0; i < MAX_HEDGEHOGS; i++)
		if (hedgehog[i])
		{
			char buf[255];
			snprintf(buf, 255, "point %i", i);
			glColor3f(1,1,1);
			hgl_print(hedgehog[i]->body.pos->x,
				  hedgehog[i]->body.pos->y,
				  buf);
		}

	ITER_OBJECT_POOL(body, &game_bodies)
	{
		body_draw(body);
	}
	ITER_END;
}

void	Game_ProcessObjects(int ticks)
{
	body_t	*body;
	int	i, bodies_count;
	vector_t	total_momentum, body_momentum;
	coord_t		avg_momentum,
			angular_momentum,
			total_angular_momentum,
			avg_angular_momentum;

	ticks = 1;

	vector_set(&total_momentum, 0, 0);
	avg_momentum = 0;
	bodies_count = 0;
	total_angular_momentum = 0;
	avg_angular_momentum = 0;

	ITER_OBJECT_POOL(body, &game_bodies)
	{
		body_process(body, ticks);

		vector_mul(body->speed, body->mass, &body_momentum);
		vector_inc(&total_momentum, &body_momentum);
		avg_momentum += vector_len(&body_momentum);
		bodies_count++;
		angular_momentum = vector_cross(&body_momentum, body->pos);
		total_angular_momentum += angular_momentum;
		avg_angular_momentum += abs(angular_momentum);
	}
	ITER_END

	{
		char	buf[255];
		snprintf(buf, 255, "total momentum: %f",
			 vector_len(&total_momentum));
		hgl_print(300, 10, buf);
		snprintf(buf, 255, "avg. momentum: %f",
			 avg_momentum / bodies_count);
		hgl_print(300, 20, buf);
/*		snprintf(buf, 255, "total angular momentum: %f",
			 total_angular_momentum);
		hgl_print(300, 30, buf);
		snprintf(buf, 255, "avg angular momentum: %f",
			 avg_angular_momentum / bodies_count);
			 hgl_print(300, 40, buf);*/
	}
}
