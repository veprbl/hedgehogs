#ifndef __F_TGA_H_
#define __F_TGA_H_

#include "h_common.h"

typedef struct {
	byte id_lenght;
	byte color_map;
	byte image_type;
	
	byte some_specs[4]; // Color Map Specification
	
	unsigned short x_origin, y_origin, width, height;
	
	byte pixel_depth;
	byte image_descriptor;
} tga_header_t;

int LoadTGA( byte *buf, unsigned int size, int *width, int *height, char *bytes_per_pixel, byte **image );

#endif
