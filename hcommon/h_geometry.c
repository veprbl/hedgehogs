#include <stdlib.h>
#include <math.h>

#include "h_common.h"
#include "h_geometry.h"

object_pool_t	vectors_pool = OBJECT_POOL_INITIALIZER;

void	vector_inc_ref_count( vector_t *vector )
{
	object_pool_push(&vectors_pool, vector);
}

vector_t*	vector_ctor( coord_t x, coord_t y )
/* Don't forget to increase ref count if you want to call destructor! */
{
	point_t	*vector;

	vector = malloc(sizeof(*vector));

	vector_set(vector, x, y);

	return vector;
}

void	vector_dtor( vector_t *vector )
{
	object_pool_free(&vectors_pool, vector);
}

point_t*	point_ctor( coord_t x, coord_t y )
{
	return (point_t*)vector_ctor(x, y);
}

void	vector_set( vector_t *vector, coord_t x, coord_t y )
{
	vector->x = x;
	vector->y = y;
}

void	vector_copy( vector_t *dest, const vector_t *src )
{
	dest->x = src->x;
	dest->y = src->y;
}

void	vector_add( const vector_t *a, const vector_t *b, vector_t *result )
{
	result->x = a->x + b->x;
	result->y = a->y + b->y;
}

void	vector_inc( vector_t *accum, const vector_t *add )
{
	vector_add(accum, add, accum);
}

void	vector_sub( const vector_t *a, const vector_t *b, vector_t *result )
{
	vector_set(result, a->x - b->x, a->y - b->y);
}

void	vector_dec( vector_t *vec, const vector_t *dec )
{
	vector_sub(vec, dec, vec);
}

void	vector_mul( const vector_t *a, float b, vector_t *result )
{
	vector_set(result, a->x * b, a->y * b);
}

coord_t	vector_cross( const vector_t *v1, const vector_t *v2 )
{
	return (v1->x) * (v2->y) - (v1->y) * (v2->x);
}

int	vector_half_plane( const vector_t *v1, const vector_t *v2 )
{
	coord_t	half_plane_sign;
	
	half_plane_sign = vector_cross(v1, v2);

	if (half_plane_sign > 0)
	{
		return CLOCKWISE;
	}
	else if (half_plane_sign < 0)
	{
		return COUNTER_CLOCKWISE;
	}
	else// if (half_plane_sign == 0)
	{
		return NON_ROTATING;
	}
}

coord_t	vector_dot( const vector_t *a, const vector_t *b )
{
	return a->x * b->x + a->y * b->y;
}

coord_t	vector_len_sqr( const vector_t *vector )
{
	return vector_dot(vector, vector);
}

coord_t	vector_len( const vector_t *vector )
{
	return sqrt(vector_len_sqr(vector));
}

coord_t	point_dist_sqr( const point_t *p1, const point_t *p2 )
{
	vector_t	t;

	vector_sub(p1, p2, &t);

	return vector_len_sqr(&t);
}

void	vector_normal( const vector_t *vector, vector_t *norm )
{
	norm->x = -vector->y;
	norm->y = vector->x;
}

void	vector_rotate( vector_t *vector, float angle )
/* rotate vector clockwise */
{
	angle = angle / 180 * M_PI;

	vector_set(vector,
		   cos(angle) * vector->x - sin(angle) * vector->y,
		   sin(angle) * vector->x + cos(angle) * vector->y);
}

void	vector_project( const vector_t *direction,
			const vector_t *vector,
			vector_t *result )
{
	if ((direction->x == 0) && (direction->y == 0))
	{
		vector_set(result, 0, 0);
	}
	else
	{
		vector_mul(direction,
			   vector_dot(direction, vector) 
			   / vector_len_sqr(direction),
			   result);
	}
}

hboolean	line_segment_intersects( const point_t *p11,
					 const point_t *p12,
					 const point_t *p21,
					 const point_t *p22,
					 point_t *intersection_point )
{
	hboolean	result = false;
	vector_t	vec, vec1, vec2, vec12;

	vector_sub(p12, p11, &vec);
	vector_sub(p21, p11, &vec1);
	vector_sub(p22, p11, &vec2);

	if (vector_half_plane(&vec, &vec1) * vector_half_plane(&vec, &vec2)
		<= 0)
	{
		vector_sub(p22, p21, &vec);
		vector_sub(p11, p21, &vec1);
		vector_sub(p12, p21, &vec2);

		result = (vector_half_plane(&vec, &vec1) * 
			  vector_half_plane(&vec, &vec2)) <= 0;

		if (result && intersection_point)
		{
			vector_t ox; /* point of intersection of line
					p21-p22 and x axis */
			const vector_t *origin;

			origin = p11;
			vector_sub(p12, origin, &vec);
			vector_sub(p21, origin, &vec1);
			vector_sub(p22, origin, &vec2);
			vector_sub(&vec2, &vec1, &vec12);

			ox.y = 0;

			if (vec12.y == 0)
			{
				origin = p21;
				vector_sub(p22, origin, &vec);
				vector_sub(p11, origin, &vec1);
				vector_sub(p12, origin, &vec2);
				vector_sub(&vec2, &vec1, &vec12);
				if (vec12.y == 0)
				{
					/* This case handled in inintuintively
					   way */
					vector_copy(intersection_point, p22);
				}
			}
	     /* FIXME: vec.y == 0 || vec.x == 0 case is not imlemented */

			ox.x = vec1.x - vec1.y / vec12.y * vec12.x;

			intersection_point->y = origin->y +
				ox.x 
				/ (vec.x / vec.y - vec12.x / vec12.y);
			intersection_point->x = origin->x +
				ox.x
				/ (1 - vec12.x * vec.y 
				   / vec12.y / vec.x);

/*DEBUG			printf("%f %f\n", intersection_point->x,
			intersection_point->y);*/
		}
	}

	return result;
}
