#include <GL/gl.h>
#include "h_common.h"

#ifndef __H_TEXTURES_H_
#define __H_TEXTURES_H_

typedef enum {
	RGB = GL_RGB,
	RGBA = GL_RGBA,
	BGR
#ifdef GL_BGR
	= GL_BGR
#endif
,
	BGRA
#ifdef GL_BGRA
	= GL_BGRA
#endif
} color_fmt_t;
typedef	GLuint texture_id_t;

typedef struct texture {
	texture_id_t	id;
	int	width, height;
	color_fmt_t    color_fmt;
} texture_t;

texture_t*	texture_ctor( void *data, color_fmt_t color_fmt, int width, int height );
texture_t*	texture_load( char *filename );
void	texture_bind( const texture_t *texture );

#endif
