#include <stdio.h>
#include <stdarg.h>
#include "h_console.h"
#include "h_common.h"

void Con_Init()
{
	fclose(fopen("logfile.txt", "w"));
}

void Con_Print(char *txt)
{
	FILE *fp = fopen("logfile.txt", "a");
	fprintf(fp, "%s", txt);
	fprintf(stderr, "%s", txt);
	fclose(fp);
}

void Con_Printf(char *fmt, ...)
{
	va_list		argptr;
	char		txt[CON_PRINTF_MAX];

	va_start(argptr, fmt);
	vsprintf(txt, fmt, argptr);
	va_end(argptr);

	Con_Print(txt);
}

