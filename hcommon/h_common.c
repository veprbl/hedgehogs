#include "stdlib.h"

#include "h_common.h"
#include "h_console.h"
#include "h_net.h"
#include "../client/client.h"
#include "h_game.h"

void	Hcommon_Init ( int argc, char **argv )
{
	Con_Init();
	NET_Init();

	CL_Init();
	Game_Init();
}

void	Hcommon_Frame()
{
	NET_Process();
}

void	Hcommon_Shutdown ( void )
{
	Game_Shutdown();
	CL_Shutdown();

	NET_Shutdown();
}

void	object_pool_push( object_pool_t *pool, void *object )
/* If object is already in pool just increase ref count, otherwise add him. */
{
	while(pool->next)
	{
		pool = pool->next;
		if (pool->object == object)
		{
			pool->ref_count++;
			return;
		}
	}

	pool->next = (struct object_pool*)malloc(sizeof(pool->next));
	pool->next->next = NULL;
	pool->next->object = object;
	pool->next->ref_count = 1;
}

unsigned int	object_pool_remove( object_pool_t *pool, void *object )
/* If object is already in pool just decrease ref count, otherwise remove it. */
{
	unsigned int	ref_count;
	object_pool_t	*prev;

	while(pool->next)
	{
		prev = pool;
		pool = pool->next;

		if (pool->object == object)
		{
			if (pool->ref_count != 0)
			{
				pool->ref_count--;
			}

			ref_count = pool->ref_count;

			if (pool->ref_count == 0)
			{
				prev->next = pool->next;
				free(pool);
			}

			return ref_count;
		}
	}
	return 0;
}

void	object_pool_free( object_pool_t *pool, void *object )
{
	if (object_pool_remove(pool, object) == 0)
	{
		free(object);
	}
}

void	object_pool_remove_all( object_pool_t *pool )
{
	object_pool_t	*root_pool = pool, *prev = NULL;

	while(pool->next)
	{
		prev = pool;
		pool = pool->next;

		free(pool->object);

		if (prev && prev != root_pool)
		{
			free(prev);
		}
	}
}
