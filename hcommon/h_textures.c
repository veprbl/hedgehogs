#include <stdlib.h>
#include "h_textures.h"
#include "h_fs.h"

texture_t*	texture_ctor( void *data, color_fmt_t color_fmt, int width, int height )
{
	texture_t	*new_texture;

	new_texture = malloc(sizeof(*new_texture));

	new_texture->color_fmt = color_fmt;
	new_texture->width = width;
	new_texture->height = height;

	glGenTextures(1, &new_texture->id);
	glBindTexture(GL_TEXTURE_2D, new_texture->id);

	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
		     width, height,
		     0, color_fmt,
		     GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	return new_texture;
}
#include <stdio.h>
texture_t*	texture_load( char *filename )
{
	texture_t*	new_texture;
	color_fmt_t	color_fmt;
	fsize_t	size;
	void	*buf, *image;
	int width, height;
	file_handle_t texture_file = NULL;
	
	buf = fs_mmap_file(filename, &texture_file, &size);
	if (buf)
	{
		if (load_tga(buf, size, &width, &height, &color_fmt, &image))
		{
			new_texture = texture_ctor(image, color_fmt, width, height);
			free(image);
		}
		else
		{
			fprintf(stderr, "Failed to load '%s'", filename);
		}
	}
	else
	{
		fprintf(stderr, "Failed to load '%s'", filename);
	}
	fs_munmap_file(buf, &texture_file);
	return new_texture;
}

void	texture_bind( const texture_t *texture )
{
	glBindTexture(GL_TEXTURE_2D, texture->id);
}
