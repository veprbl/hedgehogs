#include "h_common.h"

#ifndef __H_GEOMETRY_H_
#define __H_GEOMETRY_H_

typedef float	coord_t;

typedef struct {
	coord_t x, y;
} point_t;

typedef point_t vector_t;

/* definitions for vector_half_plane function */
#define	CLOCKWISE	1
#define NON_ROTATING	0
#define COUNTER_CLOCKWISE	-1

void	vector_inc_ref_count( vector_t *vector );
vector_t*	vector_ctor( coord_t x, coord_t y );
void	vector_dtor( vector_t *vector );
point_t*	point_ctor( coord_t x, coord_t y );
void	vector_set( vector_t *vec, coord_t x, coord_t y );
void	vector_copy( vector_t *dest, const vector_t *src );
void	vector_add( const vector_t *a, const vector_t *b, vector_t *result );
void	vector_inc( vector_t *accum, const vector_t *add );
void	vector_sub( const vector_t *a, const vector_t *b, vector_t *result );
void	vector_dec( vector_t *vec, const vector_t *dec );
void	vector_mul( const vector_t *a, float b, vector_t *result );
coord_t	vector_cross( const vector_t *v1, const vector_t *v2 );
int	vector_half_plane( const vector_t *v1, const vector_t *v2 );
coord_t	vector_dot( const vector_t *a, const vector_t *b );
coord_t	vector_len_sqr( const vector_t *vector );
coord_t	vector_len( const vector_t *vector );
coord_t	point_dist_sqr( const point_t *p1, const point_t *p2 );
void	vector_normal( const vector_t *vector, vector_t *norm );
void	vector_project( const vector_t *direction,
			const vector_t *vector,
			vector_t *result );
void	vector_rotate( vector_t *vector, float angle );
hboolean	line_segment_intersects( const point_t *p11,
					 const point_t *p12,
					 const point_t *p21,
					 const point_t *p22,
					 point_t *intersection_point );

#endif
