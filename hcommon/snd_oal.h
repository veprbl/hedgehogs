#ifndef __SND_OAL_H_
#define __SND_OAL_H_

#include <AL/al.h>
#include <AL/alc.h>

typedef struct oal_state {
	ALCdevice	*device;
	ALCcontext	*context;
} oal_state_t;

#endif
