#ifndef __H_COMMON_H_
#define __H_COMMON_H_

#ifdef __linux
#include <endian.h>
#else
#ifdef __unix
#include <machine/endian.h>
#endif
#endif
#ifdef _WIN32
#define LITTLE_ENDIAN
#endif

#define	MAXFILE		1024
#define	QHED_VERSION	1

typedef struct object_pool {
	struct object_pool	*next;
	void	*object;
	unsigned int	ref_count;
} object_pool_t;

#define OBJECT_POOL_INITIALIZER {NULL, NULL, 0}

typedef enum {false, true}	hboolean;
typedef	unsigned char	byte;

#ifdef LITTLE_ENDIAN
#define	LITTLE_ENDIAN_SHORT(i)	(i)
#define	LITTLE_ENDIAN_LONG(i)	(i)
#define	BIG_ENDIAN_SHORT(i)	(((i&0xFF)<<8)|((i>>8)&0xFF))
#define	BIG_ENDIAN_LONG(i)			\
(						\
	((i & 0xFF) << 24) |			\
	((i & 0xFF00) << 8) |			\
	((i & 0xFF0000) >> 8) |			\
	((i & 0xFF000000) >> 24)		\
)
#else
#ifdef BIG_ENDIAN
#define	LITTLE_ENDIAN_SHORT(i)	(((i&0xFF)<<8)|((i>>8)&0xFF))
#define	LITTLE_ENDIAN_LONG(i)			\
(						\
	((i & 0xFF) << 24) |			\
	((i & 0xFF00) << 8) |			\
	((i & 0xFF0000) >> 8) |			\
	((i & 0xFF000000) >> 24)		\
)
#define	BIG_ENDIAN_SHORT(i)	(i)
#define	BIG_ENDIAN_LONG(i)	(i)
#endif
#endif

void	Hcommon_Init ( int argc, char **argv );
void	Hcommon_Shutdown ( void );

void	object_pool_push( object_pool_t *pool, void *object );
unsigned int	object_pool_remove( object_pool_t *pool, void *object );
void	object_pool_free( object_pool_t *pool, void *object );
void	object_pool_remove_all( object_pool_t *pool );

#define	ITER_OBJECT_POOL(obj, pool)					\
	{ object_pool_t *_item;						\
	for(_item = (pool)->next, (obj) = _item->object;		\
	    _item;							\
	    _item = _item->next, (obj) = _item ? _item->object : NULL)
	

#define ITER_END \
	}

#endif
