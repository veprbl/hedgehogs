#include <stdio.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <signal.h>

#include "l_gl.h"
#include "../hcommon/h_gl.h"
#include "../hcommon/h_common.h"
#include "../hcommon/h_game.h"

extern hboolean down;
int fup();
extern hboolean	physics;
extern int	mouse_x, mouse_y;

int	Sys_Milliseconds()
{
	struct timeval	tv;
	gettimeofday(&tv, NULL);

	return tv.tv_sec * 1000 + tv.tv_usec / 1000;
}

int	main( int argc, char **argv )
{
	XEvent	event;
	int	running = true;
	vector_t result;

	Hcommon_Init(argc, argv);

	while(running)
	{
		while( XPending(glx_state.pDisplay) )
		{
			XNextEvent( glx_state.pDisplay, &event );

			switch( event.type )
			{
			case MotionNotify:
			{
				point_t p = {event.xmotion.x, event.xmotion.y};
				mouse_x = event.xmotion.x;
				mouse_y = event.xmotion.y;
			}
			case ButtonPress:
				if( event.xbutton.button == 1 )
				{
					point_t p = {event.xmotion.x, event.xmotion.y};
					shoot(&p);
				}
				break;

			case KeyPress:
			{
				switch (event.xkey.keycode)
				{
				case 9: //ESC
					running = false;
					break;

				case 65: //SPACE
					physics = !physics;
					break;

				case 111: //UP
					fup();
					break;

				case 116: //DOWN
					down = 1;
					break;
				}
/*DEBUG			printf("KeyPress event=%i\n", event.xkey.keycode );*/
			}
			break;

			case KeyRelease:
			{
				switch (event.xkey.keycode)
				{
				case 116: //DOWN
					down = 0;
					break;
				}
			}
			break;


			case ConfigureNotify:
			{
				hgl_reshape( event.xconfigure.width, event.xconfigure.height );
			}
			break;

			case ClientMessage:
			{
				if ((event.xclient.message_type == wm_protocols) || (event.xclient.data.l[0] == wm_delete_window))
				{
					running = false;
					break;
				}
			}
			break;
			}
		}

		glClear(GL_COLOR_BUFFER_BIT);
		glLoadIdentity();

		{
			char buf[255];
			static int prev=0,prev2, new=0, kadr = 0;
			float fps;
			point_t com;
			float area;
			prev2 = new;
			new = Sys_Milliseconds();
			if (prev == 0) prev=new,prev2=new;
			kadr++;
			if ((new - prev) >= 1000)
			{
				fps = kadr * 1000.0/(new-prev);
				kadr = 0;
				prev = new;
			}

			glColor3f(1.0f, 1.0f, 1.0f);
			sprintf(buf, "FPS: %.1f      %i", fps,new-prev2);
			hgl_print(20, 20, buf);

			glBegin(GL_POINTS);
			glVertex2i(com.x, com.y);
			glEnd();

			Game_ProcessObjects(new-prev2);

		}

		Hcommon_Frame();
		Game_DrawObjects();

		if( glx_state.DoubleBuffered )
			glXSwapBuffers( glx_state.pDisplay, glx_state.window );
		else
			glFlush(); 
	}

	Hcommon_Shutdown();

	XCloseDisplay(glx_state.pDisplay);
	return 0;
}
