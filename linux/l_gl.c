#include <string.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include "l_gl.h"

glxstate_t	glx_state;
Atom		wm_delete_window, wm_protocols;

int	VID_CreateWindow(int width, int height, hboolean fullscreen)
{
	XVisualInfo *pXVisualInfo = NULL;
	XSetWindowAttributes windowattributes;
	Colormap colormap;
	int errorBase;
	int eventBase;

	int doubleBufferVisual[] =
		{
			GLX_RGBA,
			GLX_DEPTH_SIZE, 16,
			GLX_DOUBLEBUFFER,
			None
		};

	int singleBufferVisual[] =
		{
			GLX_RGBA,
			GLX_DEPTH_SIZE, 16,
			None
		};

	glx_state.pDisplay = XOpenDisplay(NULL);

	if(glx_state.pDisplay == NULL)
	{
		Con_Print("Couldn't open display!\n");
		return false;
	}
	
	if(!glXQueryExtension(glx_state.pDisplay, &errorBase, &eventBase))
	{
		Con_Print("X server has no OpenGL GLX extension\n");
		return false;
	}

	pXVisualInfo = glXChooseVisual(glx_state.pDisplay,
				       DefaultScreen(glx_state.pDisplay),
				       doubleBufferVisual);

	glx_state.DoubleBuffered = (pXVisualInfo != NULL);
	if(pXVisualInfo == NULL)
	{
		pXVisualInfo = glXChooseVisual(glx_state.pDisplay,
					       DefaultScreen(
						       glx_state.pDisplay
						       ),
					       singleBufferVisual);

		if(pXVisualInfo == NULL)
		{
			Con_Print("Couldn't find any appropritate visual");
			return false;
		}
	}

	Con_Print("Creating rendering context...\n");
	glx_state.glxContext = glXCreateContext(glx_state.pDisplay,
						pXVisualInfo,
						NULL,
						true);

	if(glx_state.glxContext == NULL)
	{
		Con_Print("Error creating rendering context!\n");
		return false;
	}

	colormap = XCreateColormap(glx_state.pDisplay,
				   RootWindow(glx_state.pDisplay,
					      pXVisualInfo->screen),
				   pXVisualInfo->visual,
				   AllocNone);

	windowattributes.colormap     = colormap;
	windowattributes.border_pixel = 0;
	windowattributes.event_mask   = ExposureMask			|
/*		VisibilityChangeMask	|*/
		KeyPressMask		|
		KeyReleaseMask		|
		ButtonPressMask		|
		ButtonReleaseMask	|
		PointerMotionMask	;
/*		StructureNotifyMask	|
		SubstructureNotifyMask	|
		FocusChangeMask;*/

	glx_state.window = XCreateWindow(glx_state.pDisplay, 
					 RootWindow(glx_state.pDisplay, 
						    pXVisualInfo->screen), 
					 0, 0,
					 width, height,
					 0,
					 pXVisualInfo->depth,
					 InputOutput,
					 pXVisualInfo->visual,
					 CWBorderPixel	| 
					 CWColormap	| 
					 CWEventMask,
					 &windowattributes );

	XSetStandardProperties(glx_state.pDisplay,	glx_state.window,
			       "Quake Hedgehogs",	"Quake Hedgehogs",
			       None,	0,	0,	NULL);

	wm_delete_window =
		XInternAtom(glx_state.pDisplay, "WM_DELETE_WINDOW", 1);
	wm_protocols =
		XInternAtom(glx_state.pDisplay, "WM_PROTOCOLS", 1); 

	XSetWMProtocols(glx_state.pDisplay,
			glx_state.window,
			&wm_delete_window,
			1); 

	glXMakeCurrent(glx_state.pDisplay,
		       glx_state.window,
		       glx_state.glxContext);

	XMapWindow(glx_state.pDisplay, glx_state.window);
}

void	hgl_font_init()
{
	XFontStruct	*fontInfo;

	main_font = glGenLists(96);
	fontInfo = XLoadQueryFont(glx_state.pDisplay, "-*-terminal-medium-r-normal-*-18-*-*-*-p-*-*");
	if (fontInfo == NULL) {
		fontInfo = XLoadQueryFont(glx_state.pDisplay, "fixed");
		if (fontInfo == NULL) {
			Con_Printf("no X font available?");
		}
	}

	glXUseXFont(fontInfo->fid, 32, 96, main_font);
	XFreeFont(glx_state.pDisplay, fontInfo);
}

hboolean	VID_Init()
{
	VID_CreateWindow(gl_state.width, gl_state.height, gl_state.fullscreen);
	hgl_init();
}

void	VID_Shutdown()
{

}

