#ifndef __L_GL_H_
#define __L_GL_H_

#include "../hcommon/h_common.h"
#include "../hcommon/h_gl.h"

typedef struct
{
	Display		*pDisplay;
	Window		window;
	GLXContext	glxContext;

	hboolean	DoubleBuffered;
} glxstate_t;

extern	glxstate_t	glx_state;
extern	Atom		wm_delete_window, wm_protocols;

#endif
