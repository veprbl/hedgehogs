#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "../hcommon/h_fs.h"

void	*fs_mmap_file( const char *filename, file_handle_t *file_handle, fsize_t *size )
{
	int	fd;
	struct stat	fi;
	void	*data;
	char	buf[MAXFILE];
	
	fd = open(filename, O_RDONLY);
	
	if (fd == -1)
	{
		sprintf(buf, "baseqhed/%s", filename);
		fd = open(buf, O_RDONLY);
		
		if (fd == -1)
		{
			Con_Printf("Can't open file '%s'!\n", filename);
			return NULL;
		}
	}
	
	*file_handle = malloc(sizeof(fd));
	*(int *)(*file_handle) = fd;
	
	if (fstat(fd, &fi))
	{
		Con_Printf("Can't stat for '%s'\n", filename);
		return NULL;
	}
	
	if (size)
	{
		*size = fi.st_size;
	}

	data = mmap(NULL, fi.st_size, PROT_READ, MAP_PRIVATE, fd, 0);

	if (data == MAP_FAILED)
	{
		Con_Printf("Can't map file '%s'!\n", filename);
		return NULL;
	}

	return data;
}

void fs_munmap_file( char *buf, file_handle_t *file_handle )
{
	struct stat	fi;

	if (fstat(*(int*)(* file_handle), &fi) == -1)
	{
		Con_Print("Can't stat for unloading file!");
		return;
	}

	if (buf != NULL)
	{
		munmap(buf, fi.st_size);
	}

	close(*(int*)(* file_handle));

	free(*file_handle);
}
