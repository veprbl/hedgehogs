#include "client.h"

void CL_Init()
{
	VID_Init();
	SND_Init();
}

void CL_Shutdown()
{
	SND_Shutdown();
	VID_Shutdown();
}
