#include <stdio.h>
#include "../hcommon/h_polygons.h"

void glBegin(){}
void glTexCoord2f(float a, float b){}
void glEnd(){}
void glVertex2f(float a, float b){}
void hgl_print(float x, float y, char *txt){}

int	main(int argc, char **argv)
{
	int	error_count = 0;
	polygon_t	*poly;
	vertex_t	*vertex;

	printf("Testing polygons... ");

	poly = polygon_ctor();
	
	polygon_add_vertex(poly, vertex_ctor_from_coord(0,0));
	polygon_add_vertex(poly, vertex_ctor_from_coord(100,0));
	polygon_add_vertex(poly, vertex_ctor_from_coord(160,50));
	polygon_add_vertex(poly, vertex_ctor_from_coord(100,100));
	polygon_add_vertex(poly, vertex_ctor_from_coord(0,100));

// [0]
	vertex = vertex_ctor_from_coord(1000,1000);

	if (polygon_add_vertex(poly, vertex) != vertex)
	{
		printf("\n[0] polygon_add_vertex() must return passed vertex!");
		error_count++;
	}

	vertex = polygon_add_vertex(poly, vertex_ctor_from_coord(40,50));

// [1]
	if (polygon_get_vertices_count(poly) != 7)
	{
		printf("\n[1] polygon_count_vertex() gave %i (must be 7)!", polygon_get_vertices_count(poly));
		error_count++;
	}
// [2]
	if ((polygon_offset_vertex(poly, 0)->_->x != 0.0) || (polygon_offset_vertex(poly, -1)->_->y != 50.0) || (polygon_offset_vertex(poly, 3)->_->x != 100.0))
	{
		printf("\n[2] polygon_offset_vertex() gives wrong result!");
		error_count++;
	}
// [3]
	polygon_remove_vertex(poly, polygon_offset_vertex(poly, -2));

	if (polygon_offset_vertex(poly, 5)->_->x != 40)
	{
		printf("\n[3] polygon_remove_vertex() failed!");
		error_count++;
	}
// [4]
	if ((polygon_offset_vertex(poly, 6) != poly->first) || (polygon_offset_vertex(poly, -6) != poly->first))
	{
		printf("\n[4] polygon_offset_vertex() gives wrong result!");
		error_count++;
	}
// [6]
	polygon_reverse(poly);
	if ((polygon_offset_vertex(poly, 5)->_->x != 0) || (polygon_offset_vertex(poly, 0)->_->y != 50) || (polygon_offset_vertex(poly, -4)->_->x != 100))
	{
		printf("\n[6] polygon_reverse() failed!");
		error_count++;
	}
// [8]
	if (polygon_find_vertex(poly, vertex) != 0)
	{
		printf("\n[8] polygon_find_vertex() gives wrong result! (possible that it's polygon_reverse() fault)");
		error_count++;
	}
// [9]
	if (!error_count)
	{
		printf("ok\n");
		return 0;
	}
	printf("\n");
	return 1;
}
