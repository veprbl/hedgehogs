#include <stdlib.h>
#include <stdio.h> /* for snprintf */

#include "g_hedgehog.h"
#include "../hcommon/h_common.h"
#include "../hcommon/h_geometry.h"

const vector_t	head_relative_pos = {1, -9};
const vector_t	arm1_relative_pos = {8, 6};
const vector_t	arm2_relative_pos = {8, 6};
const vector_t	leg1_relative_pos = {2, 23};
const vector_t	leg2_relative_pos = {8, 23};

int		local_hedgehog = 0;
hedgehog_t	*hedgehog[MAX_HEDGEHOGS];
texture_t	*hedgehog_tex;

void	hedgehog_head_process( body_part_t *self, int ticks )
{
	vector_t	trash;
	vector_copy(self->body.pos, self->parent->pos);
	vector_add(self->body.pos, &self->relative_pos, self->body.pos);
}

void	hedgehog_head_draw( const body_part_t *self )
{
//	hglDrawRectangleSprite(hedgehog_tex, NULL, 31, 62, 11, 15);
}

const body_type_t
Hedgehog_Head = CLASS_BODY(
	hedgehog_head_process,
	hedgehog_head_draw,
	NULL,
	NULL
	);

void	hedgehog_arm_process( body_part_t *self, int ticks )
{
	vector_copy(self->body.pos, self->parent->pos);
	vector_add(self->body.pos, &self->relative_pos, self->body.pos);
}

void	hedgehog_arm_draw( const body_part_t *self )
{
//	hglDrawRectangleSprite(hedgehog_tex, NULL, 31, 0, 6, 8);
}

const body_type_t
Hedgehog_Arm = CLASS_BODY(
	hedgehog_arm_process,
	hedgehog_arm_draw,
	NULL,
	NULL
	);

void	hedgehog_leg_process( body_part_t *self, int ticks )
{
	vector_t trash;

	vector_copy(self->body.pos, self->parent->pos);
	vector_add(self->body.pos, &self->relative_pos, self->body.pos);
/*	trash.x = 3.5;
	trash.y = 8;
	vector_inc(self->body.pos, &trash);
	material_point_accel(self->body.pos, self->body.speed, ticks);
	trash.x = 3.5;
	trash.y = 8;
	vector_dec(self->body.pos, &trash);*/
}

void	hedgehog_leg_draw( const body_part_t *self )
{
//	hglDrawRectangleSprite(hedgehog_tex, NULL, 31, 29, 8, 7);
}

const body_type_t
Hedgehog_Leg = CLASS_BODY(
	hedgehog_leg_process,
	hedgehog_leg_draw,
	NULL,
	NULL
	);

void	hedgehog_draw( const hedgehog_t *self )
{
//	hglDrawRectangleSprite(hedgehog_tex, NULL, 0, 0, 20, 15);

	{
		char	buf[255];
		snprintf(buf, 255, "%f %f", self->body.pos->x, self->body.pos->y);
		hgl_print(10, 10, buf);
	}
}

void	hedgehog_process( hedgehog_t *self, int ticks )
{
	point_body_polygon_interact(&self->body, map_body, ticks);
}

const body_type_t
Hedgehog = CLASS_BODY(
	hedgehog_process,
	hedgehog_draw,
	NULL,
	NULL
	);

body_part_t*	body_part_ctor( const body_type_t *body_type,
				body_t *parent,
				const vector_t *relative_pos )
{
	body_part_t     *new_body_part;

	new_body_part = malloc(sizeof(*new_body_part));
	body_create(&new_body_part->body, body_type);

/*	vector_dtor(new_body_part->body.pos);
	vector_dtor(new_body_part->body.speed);
	new_body_part->body.pos = parent->pos;
	new_body_part->body.speed = parent->speed;
	vector_inc_ref_count(parent->pos);
	vector_inc_ref_count(parent->speed);*/

	new_body_part->parent = parent;
	vector_copy(&new_body_part->relative_pos, relative_pos);

	object_pool_push(&game_bodies, new_body_part);

	return new_body_part;
}

hedgehog_t*	hedgehog_ctor()
{
	hedgehog_t	*hedgehog;

	hedgehog = malloc(sizeof(*hedgehog));

	body_create(&hedgehog->body, &Hedgehog);
	object_pool_push(&game_bodies, hedgehog);

	hedgehog->head = body_part_ctor(&Hedgehog_Head, (body_t*)hedgehog,
			 &head_relative_pos);
	hedgehog->arm1 = body_part_ctor(&Hedgehog_Arm, (body_t*)hedgehog,
			 &arm1_relative_pos);
	hedgehog->leg1 = body_part_ctor(&Hedgehog_Leg, (body_t*)hedgehog,
			 &leg1_relative_pos);
	hedgehog->leg2 = body_part_ctor(&Hedgehog_Leg, (body_t*)hedgehog,
			 &leg2_relative_pos);
	hedgehog->alive		= true;
	hedgehog->health	= 100;

	return hedgehog;
}

void	hedgehog_dtor( hedgehog_t *self )
{
	object_pool_free(&game_bodies, self);
}
