#include <stddef.h>
#include <math.h>

#include "g_polygons.h"
#include "../hcommon/h_common.h"
#include "../hcommon/h_game.h"

#include "../hcommon/h_gl.h"
point_t contact_points[1024];
int n = 0;

void draw_gl_debug_cross( point_t *p, float r, float g, float b )
{
	glBegin(GL_LINE_STRIP);
	glColor3f(r, g , b);
	glVertex2f(p->x-10, p->y-10);
	glVertex2f(p->x+10, p->y+10);
	glVertex2f(p->x, p->y);
	glVertex2f(p->x-10, p->y+10);
	glVertex2f(p->x+10, p->y-10);
	glVertex2f(p->x, p->y);
	glColor3f(0,0,0);
	glEnd();
}

/*debug*/
extern hboolean	physics;

void	calc_to_comoving( body_t *body,
			  point_t *body_point,
			  polygon_body_t *polygon_body,
			  int ticks,
			  vector_t	*r_local,
			  vector_t	*movement_local,
			  vector_t	*speed
	)
{
	vector_t	movement;
	vector_t	r1, r2, r1_local, r2_local;
	vector_t	r1_p, r2_p, r1_p_local, r2_p_local, r_p_local;

	/* Our body goes to polygon body frame */
	vector_sub(body->pos, polygon_body->body.pos, &r1);
	vector_copy(&r1_local, &r1);
	vector_rotate(&r1_local, -polygon_body->body.angle);

	vector_copy(&r2, body_point);
	vector_copy(&r2_local, &r2);
	vector_rotate(&r2_local,
		      -polygon_body->body.angle
		      +body->angle
		);

	vector_add(&r1_local, &r2_local, r_local);

	/**/
	vector_sub(body->pos, polygon_body->body.pos, &r1_p);

	vector_mul(body->speed, ticks / 1000.0, &movement);
	vector_inc(&r1_p, &movement);
	vector_mul(polygon_body->body.speed, ticks / 1000.0, &movement);
	vector_dec(&r1_p, &movement);

	vector_copy(&r1_p_local, &r1_p);
	vector_rotate(&r1_p_local,
		      -polygon_body->body.angle
		      -polygon_body->body.angular_freq * ticks / 1000.0
		);

	vector_copy(&r2_p, body_point);
	vector_copy(&r2_p_local, &r2_p);
	vector_rotate(&r2_p_local,
		      -polygon_body->body.angle
		      -polygon_body->body.angular_freq * ticks / 1000.0
		      +body->angle
		      +body->angular_freq * ticks / 1000.0
		);

	vector_add(&r1_p_local, &r2_p_local, &r_p_local);

	/**/
	vector_sub(r_local, &r_p_local, movement_local);

	/**/
	vector_copy(speed, &r2_local);
	vector_rotate(speed, 90);
	vector_mul(speed, body->angular_freq / 180.0 * M_PI, speed);

	{
		vector_t	t;

		vector_sub(body->speed, polygon_body->body.speed, &t);
		vector_rotate(&t, -polygon_body->body.angle);
		vector_inc(speed, &t);
	}

	{
		vector_t	t;

		vector_add(&r1_local, &r2_local, &t);
		vector_rotate(&t, 90);
		vector_mul(&t, -polygon_body->body.angular_freq / 180.0 * M_PI, &t);
		vector_inc(speed, &t);
	}
}

void	point_body_polygon_interact( body_t *body,
				     point_t *body_point,
				     polygon_body_t *polygon_body,
				     int ticks )
{
	point_t		collision_point;
	vector_t	r_local, movement_local, speed;
	vertex_t	*edge_vertex;

	if (1){ int i;
		for(i = 0; (i < n) && (i < 1024); i++)
		{
			point_t	t;
			vector_copy(&t, &contact_points[i]);
			draw_gl_debug_cross(&t, 0, 1, 1);
			vector_rotate(&t, polygon_body->body.angle);
			vector_inc(&t, polygon_body->body.pos);
			draw_gl_debug_cross(&t, 0, 1, 1);
		}
	}

	calc_to_comoving(body, body_point, polygon_body, ticks,
			 &r_local, &movement_local, &speed);


	while(edge_vertex = polygon_check_collision(&polygon_body->polygon,
						 &r_local, &movement_local,
						 &collision_point))
	{
		vector_copy(&contact_points[n%1024], &collision_point);
		n++;

		float	m1, m2;
		coord_t	r_sqr;
		vector_t	edge, edge_normal, speed_proj, impulse, r;

		vector_sub(edge_vertex->_, _next(edge_vertex)->_, &edge);
		vector_normal(&edge, &edge_normal);
		printf("normal %.2f  %.2f\n", edge_normal.x, edge_normal.y);

		if (vector_dot(&edge_normal, &speed) >= 0)
		{
			printf("broke out\nspeed %f,%f\n\n\\n\n\n", speed.x, speed.y);
			return;
		}

		vector_project(&edge_normal, &speed, &speed_proj);
		printf("proj %.2f  %.2f\n", speed_proj.x, speed_proj.y);

		vector_project(&edge, &collision_point, &r);
		r_sqr = vector_len_sqr(&r);
		m1 = body->mass;
		m2 = polygon_body->body.mass;
//			+ polygon_body->body.angular_mass / r_sqr;
		vector_mul(&speed_proj, -m1 * m2 / (m1 + m2), &impulse);

		vector_rotate(&collision_point, polygon_body->body.angle);
		vector_inc(&collision_point, polygon_body->body.pos);
		vector_rotate(&impulse, polygon_body->body.angle);

		printf("imp %.2f  %.2f", impulse.x, impulse.y);

		body_apply_impulse(body, (body_t*)polygon_body,
				   &collision_point, &impulse);

		calc_to_comoving(body, body_point, polygon_body, ticks,
				 &r_local, &movement_local, &speed);
	}
}

void	polygon_body_process( polygon_body_t *self, int ticks )
{
	vertex_t	*vertex;
	body_t		*other_body;

	ITER_OBJECT_POOL(other_body, &game_bodies)
	{
		if (other_body->type != &PolygonBody)
		{
			continue;
		}
		if (other_body == (body_t*)self)
		{
			continue;
		}

		FOR_EACH_VERTEX(vertex, &self->polygon)
		{
			point_body_polygon_interact(&self->body, vertex->_, (polygon_body_t*)other_body, ticks);
		}
		NEXT;
	}
	ITER_END;
}
