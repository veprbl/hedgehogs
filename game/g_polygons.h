#ifndef __G_POLYGONS_H_
#define __G_POLYGONS_H_

#include "../hcommon/h_game.h"

typedef struct polygon_body {
	body_t	body;
	polygon_t	polygon;
} polygon_body_t;

void	polygon_body_process( polygon_body_t *self, int ticks );

extern const body_type_t PolygonBody;
extern polygon_body_t	*map_body;
extern polygon_t	*map;

#endif
