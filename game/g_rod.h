#ifndef __G_ROD_H_
#define __G_ROD_H_

#include "../hcommon/h_game.h"

typedef struct ray_of_death {
	body_t	body;
	vector_t	direction;
	float	intensity;
} ray_of_death_t;

#define	MAX_ROD_INTENSITY	1.0

#endif
