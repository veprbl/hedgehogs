#ifndef __G_HEDGEHOG_H_
#define __G_HEDGEHOG_H_

#include "../hcommon/h_game.h"

extern const vector_t	head_relative_pos;
extern const vector_t	arm1_relative_pos;
extern const vector_t	arm2_relative_pos;
extern const vector_t	leg1_relative_pos;
extern const vector_t	leg2_relative_pos;

typedef	struct body_part {
	body_t	body;
	body_t	*parent;
	vector_t	relative_pos;
} body_part_t;

typedef struct hedgehog {
	body_t	body;
	body_part_t	*head, *arm1, *arm2, *leg1, *leg2;
	hboolean	alive;
	short int	health;
} hedgehog_t;

#define MAX_HEDGEHOGS	8

extern const body_type_t	Hedgehog;
extern hedgehog_t	*hedgehog[MAX_HEDGEHOGS];
extern int		local_hedgehog;
extern texture_t	*hedgehog_tex;

hedgehog_t*	hedgehog_ctor();
void		hedgehog_dtor( hedgehog_t *hedgehog );

#endif
