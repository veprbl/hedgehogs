#include <stddef.h>
#include <stdlib.h>
#include <math.h>

#include "g_rod.h"
#include "../hcommon/h_common.h"
#include "../hcommon/h_geometry.h"

void	particle_process( particle_t *self, int ticks )
{
	vector_inc(&self->pos, &self->speed);
}

void	particle_draw( particle_t *self )
{
	glBegin(GL_POINTS);
	hglColor(&self->color);
	glVertex2f(self->pos.x, self->pos.y);
	glEnd();
}

particle_t	particle[100];

void	ray_of_death_dtor( ray_of_death_t *ray_of_death )
{
	object_pool_free(&game_bodies, ray_of_death);
}

void	ray_of_death_draw( const ray_of_death_t *rod )
{
	glColor4f(0.0, 0.1, 1.0, rod->intensity);
	glBegin(GL_LINE_STRIP);
	glVertex2f(rod->body.pos->x, rod->body.pos->y);
	glVertex2f(rod->body.pos->x + rod->direction.x, rod->body.pos->y + rod->direction.y);
	glEnd();
	int i;
	for(i = 0; i < 100; i++)
	{
		particle[i].color.alpha = sqrt(sqrt(rod->intensity+1));
		particle_draw(&particle[i]);
	}
}

void	ray_of_death_process( ray_of_death_t *self, int ticks )
{
	int i;
	coord_t delta, delta2;

	for(i = 0; i < 100; i++)
	{
		delta = sin(i + self->intensity*40);
		delta2 = cos(i + self->intensity*40);
		particle[i].speed.x = (delta * (-self->direction.y)
				       + delta2 * self->direction.x)
			/ vector_len(&self->direction);
		particle[i].speed.y = (delta * self->direction.x
				       + delta2 * self->direction.y)
			/ vector_len(&self->direction);
		particle_process(&particle[i], ticks );
	}
	self->intensity -= 0.01;
	if (self->intensity < -10)
		ray_of_death_dtor(self);
}

const body_type_t
Ray_of_death = CLASS_BODY(
	ray_of_death_process,
	ray_of_death_draw,
	NULL,
	NULL
	);

ray_of_death_t	*ray_of_death_ctor( vector_t *destination )
{
	ray_of_death_t	*rod = malloc(sizeof(*rod));
	hboolean	collides;

	body_create((body_t*)rod, &Ray_of_death);

	rod->intensity = MAX_ROD_INTENSITY;
	vector_copy(rod->body.pos, hedgehog[local_hedgehog]->body.pos);
	vector_inc(rod->body.pos, &arm1_relative_pos);
	vector_sub(destination, rod->body.pos, &rod->direction);
	vector_mul(&rod->direction, 500/vector_len(&rod->direction),
		   &rod->direction);
	collides = NULL != polygon_check_collision(map, rod->body.pos,
					   &rod->direction, destination);
	vector_sub(destination, rod->body.pos, &rod->direction);
	if (!collides)
	{
		vector_mul(&rod->direction, 500/vector_len(&rod->direction),
			   &rod->direction);
	}

	{
		int i;
		for(i = 0; i < 100; i++)
		{
			particle[i].pos.x =
				rod->body.pos->x + i * rod->direction.x / 100.0;
			particle[i].pos.y =
				rod->body.pos->y + i * rod->direction.y / 100.0;

			if (i % 2)
			{
				particle[i].color.red = 1;
				particle[i].color.green = 1;
				particle[i].color.blue = 1;
			}
			else
			{
				particle[i].color.red = 0;
				particle[i].color.green = 0;
				particle[i].color.blue = 1;
			}

		}
	}

	object_pool_push(&game_bodies, rod);

	return rod;
}
